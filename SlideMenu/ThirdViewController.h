//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSParserFirstLevel.h"
#import "Tweet.h"

@interface ThirdViewController : UIViewController

@property (nonatomic, retain) UIImageView *customImage;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property (weak, nonatomic) IBOutlet UITableView *tweetsTableView;

@property (strong, nonatomic) RSSParserFirstLevel *xmlParser;

@end


