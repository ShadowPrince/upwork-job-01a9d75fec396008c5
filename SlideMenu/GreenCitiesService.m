//  Created by Ti Simpson on 4/1/13
//  Copyright 2013 Infinite Views Development. All rights reserved.

#import "EleventhViewController.h"
#import "GreenCitiesService.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"


@implementation GreenCitiesService

/*
 CONSTANTS
 */

NSString * const ROW = @"row";
//NSString * const METRO_AREA = @"title";

NSString * const METRO_AREA = @"metro_area";
NSString * const RANK = @"_rank";
NSString * const LOCATION = @"location";
NSString * const PHONE = @"phone";
NSString * const LINK = @"link";
NSString * const ADDRESS_1 = @"address_1";
//NSString * const ADDRESS_2 = @"address_2";
NSString * const CITY = @"city";
NSString * const STATE = @"state";
NSString * const ZIP = @"zip";

//NSString * const URL = @"http://www.infinite-views.com/CDF/RSS/rss.xml";
//ftp://infinite-views.com//CDF/RSS/rss.xml
//NSString * const URL = @"http://opendata.socrata.com/views/tx87-9ypr/rows.xml?accessType=DOWNLOAD";
NSString * const URL = @"http://www.cdfms.org/chamber/chamberadvantage/fullrss";
//NEAR ME, the second view controller, PULLS THIS RSS and Parses it for map pin locations.


@synthesize gCity,greenCities;

BOOL metroAreaFound;
BOOL rankFound;
BOOL phoneFound;
BOOL linkFound;
BOOL address_1Found;
//BOOL address_2Found;
BOOL cityFound;
BOOL stateFound;
BOOL zipFound;

-(NSMutableArray *) getGreenCities
{
    self.greenCities = [[NSMutableArray alloc] init];
    
    NSString *source = [NSString stringWithFormat:URL];
    
    NSURL *url = [[NSURL alloc] initWithString:source];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    
    [parser parse];
    
    return self.greenCities;
}



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    
    if ([elementName isEqualToString:ROW]) {
        
        self.gCity = [[GreenCity alloc] init];
    }
    else if([elementName isEqualToString:METRO_AREA])
    {
        metroAreaFound = YES;
    }
    else if([elementName isEqualToString:RANK])
    {
        rankFound = YES;
    }
    else if([elementName isEqualToString:LOCATION])
    {
        self.gCity.latitude =  [[attributeDict valueForKey:@"latitude"] floatValue];
        self.gCity.longitude = [[attributeDict valueForKey:@"longitude"] floatValue];
    }
    else if([elementName isEqualToString:PHONE])
    {
        phoneFound = YES;
    }
    else if([elementName isEqualToString:LINK])
    {
        linkFound = YES;
    }
    else if([elementName isEqualToString:ADDRESS_1])
    {
        address_1Found = YES;
    }
    // else if([elementName isEqualToString:ADDRESS_2])
    //{
    //  address_2Found = YES;
    // }
    else if([elementName isEqualToString:CITY])
    {
        cityFound = YES;
    }
    else if([elementName isEqualToString:STATE])
    {
        stateFound = YES;
    }
    else if([elementName isEqualToString:ZIP])
    {
        zipFound = YES;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(metroAreaFound)
    {
        self.gCity.name = string;
        metroAreaFound = NO;
    }
    else if(rankFound)
    {
        self.gCity.rank = string;
        rankFound = NO;
    }
    else if (phoneFound) {
        self.gCity.phone = string;
        phoneFound = NO;
    }
    else if (linkFound)
    {
        self.gCity.website = string;
        linkFound = NO;
    }
    else if (address_1Found)
    {
        self.gCity.address_1 = string;
        address_1Found = NO;
    }
    //else if (address_2Found)
    //{
    //  self.gCity.address_2 = string;
    //address_2Found = NO;
    //}
    else if (cityFound)
    {
        self.gCity.city = string;
        cityFound = NO;
    }
    else if (stateFound)
    {
        self.gCity.state = string;
        stateFound = NO;
    }
    else if (zipFound)
    {
        self.gCity.zip = string;
        zipFound = NO;
    }
    //NSLog(@"%@",string);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:ROW])
    {
        [self.greenCities addObject:self.gCity];
    }
    else if ([elementName isEqualToString:PHONE])
    {
        phoneFound = NO;
    }
    else if ([elementName isEqualToString:LINK])
    {
        linkFound = NO;
    }
    else if ([elementName isEqualToString:ADDRESS_1])
    {
        address_1Found = NO;
    }
    // else if ([elementName isEqualToString:ADDRESS_2])
    //{
    //  address_2Found = NO;
    //}
    else if ([elementName isEqualToString:CITY])
    {
        cityFound = NO;
    }
    else if ([elementName isEqualToString:STATE])
    {
        stateFound = NO;
    }
    else if ([elementName isEqualToString:ZIP])
    {
        zipFound = NO;
    }
    else if ([elementName isEqualToString:RANK])
    {
        rankFound = NO;
    }
    else if ([elementName isEqualToString:METRO_AREA])
    {
        metroAreaFound = NO;
    }
    
    
}


@end
