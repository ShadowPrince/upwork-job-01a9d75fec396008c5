//
//  GreenCitiesAppDelegate.h
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"


@interface GreenCitiesAppDelegate : NSObject <UIApplicationDelegate,MKMapViewDelegate,CLLocationManagerDelegate> {

    IBOutlet MKMapView *mapView; 
    NSMutableArray *greenCities; 
    CLLocationManager *locationManager; 
    
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic,retain) IBOutlet MKMapView *mapView; 
@property (nonatomic,retain) NSMutableArray *greenCities;
@property (strong, nonatomic) UIButton *menuBtn;
@property (strong, nonatomic) UIButton *searchBtn;



@end
