//
//  XMLParser.m
//  ParsingXMLTutorial
//
//  Created by kent franks on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RSSParserFirstLevel.h"


@implementation RSSParserFirstLevel

NSMutableString	*currentNodeContent;
NSXMLParser		*parser;
Tweet			*currentTweet;

BOOL isStatus;

- (id)loadXMLByURL:(NSString *)urlString;
{
	_tweets = [[NSMutableArray alloc] init];
    
	NSURL *url = [NSURL URLWithString:urlString];
	NSData	*data = [[NSData alloc] initWithContentsOfURL:url];
	
    parser = [[NSXMLParser alloc] initWithData:data];
	parser.delegate = self;
	
    [parser parse];
	
    return self;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	currentNodeContent = (NSMutableString *) [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if ([elementname isEqualToString:@"item"])
	{
		currentTweet = [Tweet alloc];
        isStatus = YES;
	}
    else if ([elementname isEqualToString:@"description"])
	{
        isStatus = NO;
	}
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (isStatus) //title then link originally
    {
        if ([elementname isEqualToString:@"link"])
        {
            currentTweet.link = currentNodeContent;
        }
        else if ([elementname isEqualToString:@"title"])
        {
            currentTweet.title = currentNodeContent;
        }
    }
    
	if ([elementname isEqualToString:@"item"])
	{
		[self.tweets addObject:currentTweet];
        
		currentTweet = nil;
		currentNodeContent = nil;
	}
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *arrayOfListings = [NSMutableArray array];
    NSMutableDictionary *dictionary = nil;
    
    for (Tweet *tweet in self.tweets)
    {
        dictionary = [NSMutableDictionary dictionary];
        
        [dictionary setValue:tweet.title forKey:@"TweetTitle"];
        [dictionary setValue:tweet.link forKey:@"TweetLink"];
        
        [arrayOfListings addObject:dictionary];
    }
    
    [defaults setObject:arrayOfListings forKey:@"ListingsFirstLevel"];
    [defaults synchronize];
}

@end

