//
//  RSSDetailsViewController.h
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/11/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RSSDetailsViewController : UIViewController

@property (strong, nonatomic) NSDictionary *contents;

@property (strong, nonatomic) UIButton *menuBtn;
@property (strong, nonatomic) UIButton *searchBtn;

@property (strong, nonatomic) UIImageView *customImage;

@end
