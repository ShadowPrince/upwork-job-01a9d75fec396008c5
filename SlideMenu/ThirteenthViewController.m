//
//  GreenCitiesAppDelegate.m
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import "GreenCitiesAppDelegate.h"
#import "GreenCitiesService.h" 
#import "GreenCityAnnotation.h"
#import "GreenCityAnnotationView.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"


@implementation GreenCitiesAppDelegate

@synthesize window=_window,mapView,greenCities;
@synthesize menuBtn;
@synthesize searchBtn;


    


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    
    
    
    
    self.mapView.delegate = self; 
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:self];
    [locationManager requestWhenInUseAuthorization];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    [self.mapView setShowsUserLocation:YES];
    
    GreenCitiesService *greenService = [[GreenCitiesService alloc] init];
    
    self.greenCities = [greenService getGreenCities];
    
    [self.window makeKeyAndVisible];
    return YES;
}



- (void)mapView:(MKMapView *)mv didUpdateUserLocation:(MKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation fired!");
    
    CLLocationCoordinate2D maxCoord = {-90.0f,-180.0f};
    CLLocationCoordinate2D minCoord = {90.0f, 180.0f};
    
    for(int i = 0; i<=[self.greenCities count] - 1;i++) 
    {
        GreenCity *gCity = (GreenCity *) [self.greenCities objectAtIndex:i];
        CLLocationCoordinate2D newCoord = { gCity.latitude, gCity.longitude };
        
        if(gCity.longitude > maxCoord.longitude) 
        {
            maxCoord.longitude = gCity.longitude; 
        }
        
        if(gCity.latitude > maxCoord.latitude) 
        {
            maxCoord.latitude = gCity.latitude; 
        }
        
        if(gCity.longitude < minCoord.longitude) 
        {
            minCoord.longitude = gCity.longitude; 
        }
        
        if(gCity.latitude < minCoord.latitude) 
        {
            minCoord.latitude = gCity.latitude; 
        }
        
        GreenCityAnnotation *annotation = [[GreenCityAnnotation alloc] initWithCoordinate:newCoord title:gCity.name subTitle:gCity.rank];
        
        [mv addAnnotation:annotation];
       // [annotation release];
        
    }

    
    MKCoordinateRegion region = {{0.0f, 0.0f}, {0.0f, 0.0f}};
    
    region.center.longitude = (minCoord.longitude + maxCoord.longitude) / 2.0;
    region.center.latitude = (minCoord.latitude + maxCoord.latitude) / 2.0;
    
    // calculate the span
    region.span.longitudeDelta = maxCoord.longitude - minCoord.longitude;
    region.span.latitudeDelta = maxCoord.latitude - minCoord.latitude;
    
    
    [self.mapView setRegion:region animated:YES];

}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
   // [_window release];
   // [super dealloc];
}

@end
