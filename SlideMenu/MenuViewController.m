//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "MenuViewController.h"
#import "ECSlidingViewController.h"
#import "FlipsideViewController.h"

@interface MenuViewController ()

@property (strong, nonatomic) NSArray *menu;
@property (strong, nonatomic) NSArray *section1;
@property (strong, nonatomic) NSArray *section2;


@end

@implementation MenuViewController

@synthesize menu, section1, section2;
@synthesize SidewalkViewBtn;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    //self.SidewalkViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //SidewalkViewBtn.frame = CGRectMake(200, 25, 40, 30);
    //[SidewalkViewBtn setBackgroundImage:[UIImage imageNamed:@"twitter-logo.png"] forState:UIControlStateNormal];
    //[SidewalkViewBtn addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
    //SidewalkViewBtn.hidden = YES;
    //[self.view addSubview:self.SidewalkViewBtn];
    //[SidewalkViewBtn addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menu-background.png"]];
    self.tableView.backgroundView = imageView;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.section1 = [NSArray arrayWithObjects:@"Home", @"Near Me", @"Listings",@"Search",@"Join Chamber",@"Sidewalk View", nil];
    self.section2 = [NSArray arrayWithObjects:@"Social Media",@"CDF Mobile Site", nil];
    self.menu = [NSArray arrayWithObjects:self.section1, self.section2, nil];
    
    
    [self.slidingViewController setAnchorRightRevealAmount:200.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
}



-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//-(void)viewWillLayoutSubviews{
    
  //  if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
    //    self.view.clipsToBounds = YES;
      //  CGRect screenRect = [[UIScreen mainScreen] bounds];
        //CGFloat screenHeight = screenRect.size.height;
        //self.view.frame =  CGRectMake(0, 20, self.view.frame.size.width,screenHeight-20);
        //self.view.bounds = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    //}
//}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return [self.menu count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    if (section == 0) {
        
        return [self.section1 count];
        
    } else if (section == 1) {
        
        return [self.section2 count];
    }
    
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    if (section == 0) {
        
        return @"Chamber Advantage";
        
    } else if (section == 1) {
        
        return @"Community Development";
    }

    
}



-(void) tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    // Background color
    view.tintColor = [UIColor whiteColor];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor blackColor]];
    
    
    if ([view isKindOfClass: [UITableViewHeaderFooterView class]]) {
        UITableViewHeaderFooterView* castView = (UITableViewHeaderFooterView*) view;
        UIView* content = castView.contentView;
        UIColor* color = [UIColor colorWithWhite:1 alpha:1.0]; // substitute your color here
        content.backgroundColor = color;
    }
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.section1 objectAtIndex:indexPath.row]];
    } else if (indexPath.section == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.section2 objectAtIndex:indexPath.row]];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor blueColor];
    return cell;
}



//Early attempts at intiating a segue to FlipsideViewController from MenuViewController with POI's pulling up.

//The name of a modal event segue from MenuViewController to FlipsideViewController

//- (void)nextView{
  //  [self performSegueWithIdentifier:@"MenutoFlipSideViewController" sender:self];
//}


//The name of a modal event segue from MenuViewController to FlipsideViewController

//- (void)btnclicked:(UIButton *)sender {
  //  [self performSegueWithIdentifier:@"showAlternate" sender:self];
//}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //to move to mobile website
    if (indexPath.section == 1 && indexPath.row == [[self.menu lastObject] count] - 1) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification" message:kAlertMessageVisitMobileWebsite delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = MenuAlertViewTagMobileWebSite;
        [alertView show];
        return;
    }
    
    if (indexPath.row == 7 ) {
        // [buttonObj sendActionsForControlEvents: UIControlEventTouchUpInside];
        
        // ^ Attempt to emulate a touch event of FlipSideViewController Button in MainViewController (Sidewalk View is the seventh row in the table view menu.
    }
    
    UIViewController *newTopViewController;
    if (indexPath.section == 0) {
        NSString *identifier = [NSString stringWithFormat:@"%@", [self.section1 objectAtIndex:indexPath.row]];
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    } else if (indexPath.section == 1) {
        NSString *identifier = [NSString stringWithFormat:@"%@", [self.section2 objectAtIndex:indexPath.row]];
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    }
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];
}

- (IBAction)done:(id)sender {
   // [self.modalViewController FlipsideViewController:self];
    //If working, this would link to the FlipSideViewControllers "Done" Nav Bar Button.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case MenuAlertViewTagMobileWebSite:{
            if (buttonIndex != 1) {
                return;
            }
            NSString *url = kUrlMobileSite;
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *theUrl=  [NSURL URLWithString:[NSString stringWithString:url]];
            if ([[UIApplication sharedApplication] canOpenURL:theUrl]) {
                [[UIApplication sharedApplication] openURL:theUrl];
            }
            else {
                NSLog(@"Can not open URL..");
            }
        }
        default:
            break;
    }
}


@end
