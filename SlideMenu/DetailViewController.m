//
//  DetailViewController.m
//  CDF Chamber Advantage
//
//  Created by Jack on 4/12/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import "DetailViewController.h"

#import "RSSAllListingsDetailsParser.h"

#import "MBProgressHUD.h"


@interface DetailViewController ()

@property (strong, nonatomic) NSDictionary *dictionaryDetails;

@property (assign, nonatomic, getter = isDataInitialized) BOOL dataInitialized;

@end


@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initializeData
{
    NSLog(@"%s", __FUNCTION__);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if ([defaults boolForKey:@"AllListingsParsed"] == NO)
    {
        RSSAllListingsDetailsParser *parser = [[RSSAllListingsDetailsParser alloc] init];
        
        [parser startParsing];
    }
    else
    {
        double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
        NSLog(@"timeInterval = %lf", timeInterval);
        
        if (timeInterval > 60 * 60 * 24 * 3)
        {
            RSSAllListingsDetailsParser *parser = [[RSSAllListingsDetailsParser alloc] init];
            
            [parser startParsing];
        }
    }
    
    self.dictionaryDetails = [defaults dictionaryForKey:self.listingTitle];
    
    NSLog(@"self.dictionaryDetails = %@", self.dictionaryDetails);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    self.lblName.text = self.listingTitle;
    self.lblName.text = @"";
    self.lblOffer.text = @"";
    self.discountLabel.text = @"";
    _phoneNumberLabel.text = @"";
    self.websiteLabel.text = @"";
    self.addressLabel.text = @"";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:@"AllListingsParsed"] == YES)
    {
        double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
        
        if (timeInterval < 60 * 60 * 24 * 3)
        {
            self.dataInitialized = YES;
            self.dictionaryDetails = [defaults dictionaryForKey:self.listingTitle];
            
            [self setDetails];
        }
    }
    
    
    self.scrollView.contentSize = CGSizeMake(320, 750);
    
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    self.offerBase.layer.cornerRadius = 5.0f;
    self.contactBase.layer.cornerRadius = 5.0f;
    self.discountBaseView.layer.cornerRadius = 5.0f;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if (!self.isDataInitialized)
    {
        [MBProgressHUD showHUDAddedTo:self.view
                             animated:YES];
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            // Do something...
            
            [self initializeData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view
                                     animated:YES];
                
                [self setDetails];
            });
        });
        
        // Start showing indicator
        //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
//        [self initializeData];
        
        // Stop showing indicator
        //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
//        [self setDetails];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDetails
{
    if (self.dictionaryDetails)
    {
        self.lblName.text = [self.dictionaryDetails objectForKey:@"Title"];
        
        self.lblOffer.text = [self.dictionaryDetails objectForKey:@"Rank"];
        [self.lblOffer sizeToFit];
        CGRect frame = self.offerBaseView.frame;
        frame.size.height = self.lblOffer.frame.size.height + 20;
        self.offerBaseView.frame = frame;
        
        //discount segment
        self.discountLabel.text = [self.dictionaryDetails objectForKey:@"Discount"];
        [self.discountLabel sizeToFit];
        frame = self.discountBaseView.frame;
        frame.origin.y = CGRectGetMaxY(self.offerBaseView.frame) + 10;
        frame.size.height = self.discountLabel.frame.size.height + 20;
        self.discountBaseView.frame = frame;
        
        //contact segment
        _phoneNumberLabel.text = [self.dictionaryDetails objectForKey:@"Phone"];
        self.websiteLabel.text = [self.dictionaryDetails objectForKey:@"Website"];;
        [self.websiteLabel sizeToFit];
        
        if ((self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10 >= self.addressLabel.frame.origin.y)
        {
            CGRect rect = self.addressLabel.frame;
            
            rect.origin.y = (self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10;
            self.addressLabel.frame = rect;
            rect = self.addressLabel.frame;
            rect.origin.y = (self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10;
            self.addressLabel.frame = rect;
        }
        
        NSString *addressString = [NSString stringWithFormat:@"%@ \n%@ \n%@ %@", [self.dictionaryDetails objectForKey:@"Address_1"], [self.dictionaryDetails objectForKey:@"City"], [self.dictionaryDetails objectForKey:@"State"], [self.dictionaryDetails objectForKey:@"Zip"]];
        
        self.addressLabel.text = addressString;
        [self.addressLabel sizeToFit];
        
        frame = self.contactBase.frame;
        frame.size.height = (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+15;
        frame.origin.y = CGRectGetMaxY(self.discountBaseView.frame) + 10;
        self.contactBase.frame = frame;
        
        //        [((UIScrollView *) self.view) setContentSize:CGSizeMake(320, 800)];
        
        
        //        if (self.contactBase.frame.size.height <= (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+10) {
        //            CGRect frame = self.contactBase.frame;
        //            frame.size.height = (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+10;
        //            self.contactBase.frame = frame;
        //        }

    }
}

- (IBAction)closeView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)makeCall:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification" message:kAlertMessageMakeCall delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
   alertView.tag = DetailAlertViewTagCall;
    [alertView show];
}

- (IBAction)openWebSite:(id)sender {
     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification" message:kAlertMessageVisitWebsite delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    alertView.tag = DetailAlertViewTagWebsite;
   [alertView show];
}

- (IBAction)openAddress:(id)sender {
   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notification" message:kAlertMessageVisitAddress delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
   alertView.tag = DetailAlertViewTagAddress;
   [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case DetailAlertViewTagCall:{
            //            if (buttonIndex != 1 || self.itemDetails.phone == nil) {
            if (buttonIndex != 1 || [self.dictionaryDetails objectForKey:@"Phone"] == nil) {
                return;
            }
            
            //            NSString *url = [NSString stringWithFormat:@"tel:%@",self.itemDetails.phone];
            NSString *url = [NSString stringWithFormat:@"tel:%@", [[self.dictionaryDetails objectForKey:@"Phone"] stringByReplacingOccurrencesOfString:@"." withString:@""]];
            
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *theUrl=  [NSURL URLWithString:[NSString stringWithString:url]];
            
            if ([[UIApplication sharedApplication] canOpenURL:theUrl]) {
                [[UIApplication sharedApplication] openURL:theUrl];
            }
            else {
                NSLog(@"Can not open URL..");
                [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This contanct number appears not to be valid." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        }
            break;
        case DetailAlertViewTagWebsite:{
            //            if (buttonIndex != 1 || self.itemDetails.website == nil) {
            if (buttonIndex != 1 || [self.dictionaryDetails objectForKey:@"Website"] == nil) {
                return;
            }
            
            //            NSString *url = self.itemDetails.website;
            NSString *url = [self.dictionaryDetails objectForKey:@"Website"];
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *theUrl=  [NSURL URLWithString:[NSString stringWithString:url]];
            if ([[UIApplication sharedApplication] canOpenURL:theUrl]) {
                [[UIApplication sharedApplication] openURL:theUrl];
            }
            else {
                NSLog(@"Can not open URL..");
                [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This webside url does not appear to be valid." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        }
        case DetailAlertViewTagAddress:{
            // Check for iOS 6
            //            Class mapItemClass = [MKMapItem class];
            //            if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
            //            {
            //                // Create an MKMapItem to pass to the Maps app
            //                CLLocationCoordinate2D coordinate =
            //                CLLocationCoordinate2DMake(16.775, -3.009);
            //                MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
            //                                                               addressDictionary:nil];
            //                MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
            //                [mapItem setName:@"My Place"];
            //                // Pass the map item to the Maps app
            //                [mapItem openInMapsWithLaunchOptions:nil];
            //            }
            //
            //            if (buttonIndex != 1 || self.itemDetails.website == nil) {
            if (buttonIndex != 1 || [self.dictionaryDetails objectForKey:@"Address_1"] == nil) {
                return;
            }
            
            //            NSString *url = [NSString stringWithFormat:@"http://maps.apple.com?q=%@", self.itemDetails.address_1];
            NSString *url = [NSString stringWithFormat:@"http://maps.apple.com?q=%@", [self.dictionaryDetails objectForKey:@"Address_1"]];
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *theUrl=  [NSURL URLWithString:[NSString stringWithString:url]];
            if ([[UIApplication sharedApplication] canOpenURL:theUrl]) {
                [[UIApplication sharedApplication] openURL:theUrl];
            }
            else {
                NSLog(@"Can not open URL..");
                [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This webside url does not appear to be valid." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        }
            
        default:
            break;
    }
}


- (IBAction)revealMenu:(id)sender
{
     //   [self.slidingViewController anchorTopViewTo:ECRight];
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    if (self.isComingFromMapPin)
//    {
//        
//    }
//    else
//    {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

@end


/*
 
 self.lblName.text = self.itemDetails.name;
 
 self.lblOffer.text = self.itemDetails.rank;
 [self.lblOffer sizeToFit];
 CGRect frame = self.offerBaseView.frame;
 frame.size.height = self.lblOffer.frame.size.height + 20;
 self.offerBaseView.frame = frame;
 
 //discount segment
 self.discountLabel.text = self.itemDetails.discount;
 [self.discountLabel sizeToFit];
 frame = self.discountBaseView.frame;
 frame.origin.y = CGRectGetMaxY(self.offerBaseView.frame) + 10;
 frame.size.height = self.discountLabel.frame.size.height + 20;
 self.discountBaseView.frame = frame;
 
 //contact segment
 [_phoneNumberLabel setText:self.itemDetails.phone];
 self.websiteLabel.text = self.itemDetails.website;
 [self.websiteLabel sizeToFit];
 
 if ((self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10 >= self.addressLabel.frame.origin.y)
 {
 CGRect rect = self.addressLabel.frame;
 
 rect.origin.y = (self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10;
 self.addressLabel.frame = rect;
 rect = self.addressLabel.frame;
 rect.origin.y = (self.websiteLabel.frame.origin.y + self.websiteLabel.frame.size.height) + 10;
 self.addressLabel.frame = rect;
 }
 
 NSString *addressString = [NSString stringWithFormat:@"%@ \n%@ \n%@ %@", self.itemDetails.address_1, self.itemDetails.city, self.itemDetails.state, self.itemDetails.zip];
 
 self.addressLabel.text = addressString;
 [self.addressLabel sizeToFit];
 
 frame = self.contactBase.frame;
 frame.size.height = (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+15;
 frame.origin.y = CGRectGetMaxY(self.discountBaseView.frame) + 10;
 self.contactBase.frame = frame;
 
 //        [((UIScrollView *) self.view) setContentSize:CGSizeMake(320, 800)];
 
 
 //        if (self.contactBase.frame.size.height <= (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+10) {
 //            CGRect frame = self.contactBase.frame;
 //            frame.size.height = (self.addressLabel.frame.size.height+self.addressLabel.frame.origin.y)+10;
 //            self.contactBase.frame = frame;
 //        }
 
 
 */