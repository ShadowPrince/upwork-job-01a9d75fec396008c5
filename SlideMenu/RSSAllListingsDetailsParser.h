//
//  LastParser.h
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/27/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSSAllListingsDetailsParser : NSObject <NSXMLParserDelegate>

- (id)init;
- (void)startParsing;

@end
