//
//  AppDelegate.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "RSSParserFirstLevel.h"
//#import "RSSParserSecondLevel.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) RSSParserFirstLevel *firstLevelParser;
//@property (strong, nonatomic) NSArray *listingsFirstLevel;
//
//@property (strong, nonatomic) RSSParserSecondLevel *secondLevelParser;
//@property (strong, nonatomic) NSArray *listingsSecondLevel;

@end
