//
//  LastParser.m
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/27/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import "RSSAllListingsDetailsParser.h"

#import "GreenCity.h"


#define ROW_            @"row"
#define TITLE_          @"title"
//#define METRO_AREA_     @"metro_area"
#define RANK_           @"_rank"
#define LOCATION_       @"location"
#define PHONE_          @"phone"
#define WEBSITE_        @"website"
#define ADDRESS_1_      @"address_1"
//#define ADDRESS_2 = @"address_2"
#define CITY_           @"city"
#define STATE_          @"state"
#define ZIP_            @"zip"
#define DISCOUNT_       @"discount"


@interface RSSAllListingsDetailsParser ()

@property (strong, nonatomic) GreenCity *listingItem;
@property (strong, nonatomic) NSMutableArray *allListings;

@property (strong, nonatomic) NSXMLParser *parser;

@end


@implementation RSSAllListingsDetailsParser

BOOL newRowFound;
//BOOL metroAreaFound;
BOOL titleFound;
BOOL rankFound;
BOOL phoneFound;
BOOL websiteFound;
BOOL address_1Found;
//BOOL address_2Found;
BOOL cityFound;
BOOL stateFound;
BOOL zipFound;
BOOL discountFound;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.allListings = [[NSMutableArray alloc] init];
        
        NSString *webAddress = @"http://www.cdfms.org/chamber/chamberadvantage/fullrss";
        NSURL *url = [NSURL URLWithString:webAddress];
        
        self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        
        self.parser.delegate = self;
    }
    
    return self;
}

- (void)startParsing
{
    NSLog(@"%s", __FUNCTION__);
    
    [self.parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:ROW_])
    {
        self.listingItem = [[GreenCity alloc] init];
        
        newRowFound = YES;
    }
    else if (newRowFound)
    {
        if ([elementName isEqualToString:RANK_])
        {
            rankFound = YES;
        }
        else if ([elementName isEqualToString:DISCOUNT_])
        {
            discountFound = YES;
        }
        else if ([elementName isEqualToString:TITLE_])
        {
            titleFound = YES;
        }
        else if ([elementName isEqualToString:ADDRESS_1_])
        {
            address_1Found = YES;
        }
        else if ([elementName isEqualToString:CITY_])
        {
            cityFound = YES;
        }
        else if ([elementName isEqualToString:STATE_])
        {
            stateFound = YES;
        }
        else if ([elementName isEqualToString:ZIP_])
        {
            zipFound = YES;
        }
        else if ([elementName isEqualToString:PHONE_])
        {
            phoneFound = YES;
        }
        else if ([elementName isEqualToString:WEBSITE_])
        {
            websiteFound = YES;
        }
        else if ([elementName isEqualToString:LOCATION_])
        {
            self.listingItem.latitude =  [[attributeDict valueForKey:@"latitude"] floatValue];
            self.listingItem.longitude = [[attributeDict valueForKey:@"longitude"] floatValue];
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (newRowFound)
    {
        if (titleFound)
        {
            self.listingItem.title = string;
            
            NSLog(@"sTitle = %@", self.listingItem.title);
            
            titleFound = NO;
        }
        else if (rankFound)
        {
            self.listingItem.rank = string;
            rankFound = NO;
        }
        else if (discountFound)
        {
            self.listingItem.discount = string;
            discountFound = NO;
        }
        else if (address_1Found)
        {
            self.listingItem.address_1 = string;
            address_1Found = NO;
        }
        else if (cityFound)
        {
            self.listingItem.city = string;
            cityFound = NO;
        }
        else if (stateFound)
        {
            self.listingItem.state = string;
            stateFound = NO;
        }
        else if (zipFound)
        {
            self.listingItem.zip = string;
            zipFound = NO;
        }
        else if (phoneFound)
        {
            self.listingItem.phone = string;
            phoneFound = NO;
        }
        else if (websiteFound)
        {
            self.listingItem.website = string;
            websiteFound = NO;
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:ROW_])
    {
        
        NSLog(@"GAGA self.listingItem.title = %@", self.listingItem.title);
        
        [self.allListings addObject:self.listingItem];
        
        newRowFound = NO;
    }
    else if ([elementName isEqualToString:RANK_])
    {
        rankFound = NO;
    }
    else if ([elementName isEqualToString:DISCOUNT_])
    {
        discountFound = NO;
    }
    else if ([elementName isEqualToString:ADDRESS_1_])
    {
        address_1Found = NO;
    }
    else if ([elementName isEqualToString:PHONE_])
    {
        phoneFound = NO;
    }
    else if ([elementName isEqualToString:WEBSITE_])
    {
        websiteFound = NO;
    }
    else if ([elementName isEqualToString:CITY_])
    {
        cityFound = NO;
    }
    else if ([elementName isEqualToString:STATE_])
    {
        stateFound = NO;
    }
    else if ([elementName isEqualToString:ZIP_])
    {
        zipFound = NO;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
//    NSMutableArray *arrayOfListings = [NSMutableArray array];
    NSMutableDictionary *dictionary = nil;
    
    NSLog(@"GGGGG [self.allListings count] = %d", [self.allListings count]);
    
    for (GreenCity *listing in self.allListings)
    {
        dictionary = [NSMutableDictionary dictionary];
        
        
        [dictionary setValue:listing.rank
                      forKey:@"Rank"];
        
        [dictionary setValue:listing.discount
                      forKey:@"Discount"];
        
        [dictionary setValue:listing.title
                      forKey:@"Title"];
        
        [dictionary setValue:listing.link
                      forKey:@"Link"];
        
        [dictionary setValue:listing.address_1
                      forKey:@"Address_1"];
        
        [dictionary setValue:listing.city
                      forKey:@"City"];
        
        [dictionary setValue:listing.state
                      forKey:@"State"];
        
        [dictionary setValue:listing.zip
                      forKey:@"Zip"];
        
        [dictionary setValue:listing.phone
                      forKey:@"Phone"];
        
        [dictionary setValue:listing.website
                      forKey:@"Website"];
        
        [defaults setObject:dictionary
                     forKey:listing.title];
        [defaults synchronize];
    }
        
    [defaults setBool:YES
               forKey:@"AllListingsParsed"];
    [defaults setObject:[NSDate date]
                 forKey:@"LastTimeDownloaded"];
    [defaults synchronize];
    
    NSLog(@"DATE AllListingsParsed = %@", (NSDate *) [defaults objectForKey:@"LastTimeDownloaded"]);
}

@end
