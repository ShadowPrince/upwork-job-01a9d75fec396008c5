//
//  GreenCityAnnotationView.h
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"

@interface GreenCityAnnotationView : MKAnnotationView {
    
    NSString *rank; 
    
}

@property (nonatomic,retain) NSString *rank; 

@end
