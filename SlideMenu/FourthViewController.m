//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "FourthViewController.h"

#import "RSSSecondLevelViewController.h"
#import "DetailViewController.h"

#import "ECSlidingViewController.h"
#import "MenuViewController.h"

#import "RSSDetailXMLParser.h"


@interface FourthViewController ()

@property (strong, nonatomic) NSMutableArray *arrayOfSearchItems;

@end


@implementation FourthViewController

- (NSMutableArray *)arrayOfSearchItems
{
    if (!_arrayOfSearchItems)
    {
        _arrayOfSearchItems = [NSMutableArray array];
    }
    
    return _arrayOfSearchItems;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;

    //relaod table with all the data
    [self updateArrayFor:@"" andReladTable:YES];
    
    //set table view data source
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    
    //initialize array
//    _searchContentMutableArray = [NSMutableArray array];
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    //load search xml
//    self.xmlParser = [[RSSParserFirstLevel alloc] loadXMLByURL:@"http://www.cdfms.org/chamber/chamberadvantage/rss"];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.arrayOfSearchItems = [NSMutableArray arrayWithArray:[defaults arrayForKey:@"ListingsFirstLevel"]];
    
    NSArray *listingsSecondLevel = nil;
    
//    for (NSDictionary *dictionary in self.allListings)
    for (int i = 0; i < [self.arrayOfSearchItems count]; ++i)
    {
        NSString *RSSLink = [[self.arrayOfSearchItems objectAtIndex:i] objectForKey:@"TweetLink"];
        
        listingsSecondLevel = [defaults arrayForKey:RSSLink];
    
        for (NSDictionary *obj in listingsSecondLevel)
        {
            [self.arrayOfSearchItems addObject:obj];
        }
    }
    
    NSLog(@"QQQQQ [self.allListings count] = %d", [self.arrayOfSearchItems count]);
    NSLog(@"self.allListings = %@", self.arrayOfSearchItems);
    
    
    
    
//    NSDictionary *dictionary = nil;
//    
//
//    for (int i = 0; i < [self.allListings count]; ++i)
//    {
//        dictionary = (NSDictionary *) [self.allListings objectAtIndex:i];
//        
//        [self.arrayOfSearchItems addObject:dictionary];
//        
//        RSSDetailXMLParser *robject = [[RSSDetailXMLParser alloc] init];
//        
//        //        self.contents = [robject getSubCategoryDetailsWithURL:self.RSS_Link];
//        NSMutableArray *subItems = [robject getSubCategoryDetailsWithURL:[dictionary objectForKey:@"TweetLink"]];
//        
//        for (int j = 0; j < [subItems count]; ++j)
//        {
//            GreenCity *city = (GreenCity *) [subItems objectAtIndex:j];
//            
//            //            NSLog(@"city.name = %@", city.name);
//            //            NSLog(@"city.title = %@", city.title);
//            
//            city.link = [dictionary objectForKey:@"TweetLink"];
//            
//            [self.arrayOfSearchItems addObject:city];
//        }
//    }
    
    
    
    
    
//    for (int i = 0; i < [self.xmlParser.tweets count]; ++i)
//    {
//        Tweet *tweet = (Tweet *) [self.xmlParser.tweets objectAtIndex:i];
//        
//        [self.arrayOfSearchItems addObject:tweet];
//        
//        RSSDetailXMLParser *robject = [[RSSDetailXMLParser alloc] init];
//        
////        self.contents = [robject getSubCategoryDetailsWithURL:self.RSS_Link];
//        NSMutableArray *subItems = [robject getSubCategoryDetailsWithURL:tweet.link];
//
//        for (int j = 0; j < [subItems count]; ++j)
//        {
//            GreenCity *city = (GreenCity *) [subItems objectAtIndex:j];
//            
////            NSLog(@"city.name = %@", city.name);
////            NSLog(@"city.title = %@", city.title);
//            
//            city.link = tweet.link;
//            
//            [self.arrayOfSearchItems addObject:city];
//        }
//    }

    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.listSearchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_searchContentArray count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
        cell.imageView.image = [UIImage imageNamed:@"twitter-logo.png"];
    }
    
//[object objectForKey:@"TweetTitle"]
    
//    cell.textLabel.text = [_searchContentArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [[_searchContentArray objectAtIndex:indexPath.row] objectForKey:@"TweetTitle"];
    
//    cell.textLabel.text = dictionary
    
    
//    if ([object isKindOfClass:[Tweet class]])
//    {
//        Tweet *currentTweet = (Tweet *) [_searchContentArray objectAtIndex:indexPath.row];
//        
////        contentLabel.text = [currentTweet content];
////        dateLabel.text = [currentTweet dateCreated];
//        cell.textLabel.text = [currentTweet title];
//    }
//    else if ([object isKindOfClass:[GreenCity class]])
//    {
//        GreenCity *city = (GreenCity *) [_searchContentArray objectAtIndex:indexPath.row];
//        
////        contentLabel.text = city.name;
//////        dateLabel.text = city.website;
//////        dateLabel.text = city.link;
////        dateLabel.text = city.title;
////        cell.textLabel.text = (![city.name isEqualToString:@""]) ? city.name : city.title;
//        cell.textLabel.text = city.title;
//    }
    

    return cell;
}

#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    if ([self.listSearchBar isFirstResponder])
    {
        [self.listSearchBar resignFirstResponder];
        
        return;
    }
    
    RSSSecondLevelViewController *secondLevelVC = nil;
    DetailViewController *detailsVC = nil;
    
    NSDictionary *dictionary = [_searchContentArray objectAtIndex:indexPath.row];
  
    NSString *link = [dictionary objectForKey:@"TweetLink"];
    
    if ([link hasSuffix:@"/rss"])
    {
        secondLevelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSS_SecondLevel"];
        
        secondLevelVC.title = [dictionary objectForKey:@"TweetTitle"];
        secondLevelVC.RSS_Link = [dictionary objectForKey:@"TweetLink"];

        [self.navigationController pushViewController:secondLevelVC
                                             animated:YES];
    }
    else
    {
        detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail View"];
        
        detailsVC.listingTitle = [dictionary objectForKey:@"TweetTitle"];

        [self.navigationController pushViewController:detailsVC
                                             animated:YES];
    }
}


#pragma mark - Search Bar Delegate
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{                       // called when text ends editing
    [self updateArrayFor:searchBar.text andReladTable:YES];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{   // called when text changes (including clear)
    [self updateArrayFor:searchText andReladTable:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{                     // called when keyboard search button pressed
    [self updateArrayFor:searchBar.text andReladTable:YES];
    [searchBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar{                    // called when cancel button pressed
    [self updateArrayFor:searchBar.text andReladTable:YES];
}

- (void)updateArrayFor:(NSString *)searchValue andReladTable:(BOOL)condition
{//searchValue != nil &&
    if (![searchValue length])
    {
        return;
    }

    //update search array
    _searchContentArray = nil;
    _searchContentArray = [self searchValue:searchValue inArray:self.arrayOfSearchItems];
    
    if (!condition)
    {
        return;
    }
    
    // reload table
    [self.tableView reloadData];
}

- (NSArray *)searchValue:(NSString *)value
                 inArray:(NSArray *)array
{
    NSMutableArray *searchResultArray = [NSMutableArray array];
    
    if (value == nil || ![value length])
    {
        return [NSArray arrayWithArray:self.arrayOfSearchItems];
    }
    
    NSLog(@"[searchResultArray count] = %d", [searchResultArray count]);
    
//    NSDictionary *dictionary = nil;
    
    for (id object in array)
    {
        if ([[object objectForKey:@"TweetTitle"] rangeOfString:value options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
//            [searchResultArray addObject:[object objectForKey:@"TweetTitle"]];
            [searchResultArray addObject:object];
            
        }
        
//        if ([object isKindOfClass:[Tweet class]])
//        {
//            Tweet *tweet = (Tweet *) object;
//            
//            if ([tweet.title rangeOfString:value options:NSCaseInsensitiveSearch].location != NSNotFound) {
//                [searchResultArray addObject:tweet];
//            }
//        }
//        else if ([object isKindOfClass:[GreenCity class]])
//        {
//            GreenCity *city = (GreenCity *) object;
//            
//            if ([city.name rangeOfString:value options:NSCaseInsensitiveSearch].location != NSNotFound)
//            {
//                [searchResultArray addObject:city];
//            }
//        }
    }
    
//    NSLog(@"[searchResultArray count] = %d", [searchResultArray count]);
    
    return [NSArray arrayWithArray:searchResultArray];
}

- (IBAction)revealMenu:(id)sender
{
    [self.listSearchBar resignFirstResponder];
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    [self.listSearchBar resignFirstResponder];
}

@end
