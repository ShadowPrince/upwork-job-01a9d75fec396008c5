//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSParserFirstLevel.h"


@interface FourthViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

//@property (strong, nonatomic) UIButton *menuBtn;
//@property (strong, nonatomic) UIButton *searchBtn;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RSSParserFirstLevel *xmlParser;
@property (nonatomic, retain) UIImageView *customImage;


@property (strong,nonatomic) IBOutlet UISearchBar *listSearchBar;
@property (strong,nonatomic) NSArray *searchContentArray;
@end
