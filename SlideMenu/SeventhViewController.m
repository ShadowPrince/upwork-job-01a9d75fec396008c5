//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "SeventhViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"

@interface SeventhViewController ()

@end

@implementation SeventhViewController
@synthesize menuBtn;
@synthesize searchBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"FIRED NEW NO CONNECTION DETECTED ON MAINVIEW");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"This application requires an active internet connection. Please check your devices network settings." delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show];
        
        
    } else {
        NSLog(@"There IS internet connection!!!!!!");
    }
    
    bool isAvailable = [[NSUserDefaults standardUserDefaults]boolForKey:@"ISNETAVAILABLE"];
    
    if (isAvailable) {
        
   
        
    }
    
    else {
        
        // internet is unavailable.
        
       
        
        
    }

    
    float width = self.view.bounds.size.width;
    float height = self.view.bounds.size.height;
    UIWebView *wv1 = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    wv1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    wv1.scalesPageToFit = NO;
    wv1.delegate = self;
    
    //wv1.delegate = self;
    //[self.view addSubview:wv1];
    //[self.view insertSubview:menuBtn aboveSubview:wv1];
    [self.view insertSubview:wv1 atIndex:1];
    
    
    wv1.backgroundColor = [UIColor whiteColor];
    wv1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    [wv1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.infinite-views.com//CDF/iPhone/HomeScreen/CDFMS/CDFData/www.cdfms.org/index.html"]]];
    
    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
    //    [self.view addSubview:self.menuBtn];
    
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    //Top Main Menu Search Button
    //    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    searchBtn.frame = CGRectMake(275, 25, 40, 30);
    //    [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];
    
    //[wv1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.infinite-views.com//CDF/iPhone/HomeScreen/onload/"]]];
    //The Following Button Launches the FlipSideViewController Programatically

    
	// Do any additional setup after loading the view.
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    //self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //menuBtn.frame = CGRectMake(9, 23, 40, 30);
    //[menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    //[menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
   // [self.view addSubview:self.menuBtn];
    
    
    
    //Top Main Menu Search Button
   // self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
   // searchBtn.frame = CGRectMake(275, 25, 40, 30);
   // [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
   // [searchBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self.view addSubview:self.searchBtn];
    
       
    
}



- (BOOL)webView:(UIWebView *)wv1 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return false;
    }
    return true;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

@end
