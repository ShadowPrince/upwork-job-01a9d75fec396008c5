//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//
//The Second "NEAR ME" view. Current bugs: Annotations pins won't stop animating. RSS feed is parsing incorrectly on some POI's/Pins.
//The full RSS feed it parses: http://www.cdfms.org/chamber/chamberadvantage/fullrss

#import "EleventhViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "GreenCitiesAppDelegate.h"
#import "GreenCitiesService.h"
#import "GreenCityAnnotation.h"
#import "GreenCityAnnotationView.h"
#import "DetailViewController.h"

@interface EleventhViewController ()


@end

@implementation EleventhViewController
//- (id)init
//{
//  self = [super init];
//if (0 != self) {
//  counterPlus = 0;
// }
// return self;
//}


@synthesize mapView,greenCities;

//@synthesize stbackground;


//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self.mapView setDelegate:self];
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setUserTrackingMode: MKUserTrackingModeFollow animated: YES];
    
    
    //[MKUserTrackingModeFollowWithHeading rotateEnabled: NO];
    
    
    
    
    //CLLocationCoordinate2D loc = [userLocation coordinate];
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 500, 500);
    //[self.mapView setRegion:region animated:YES];
    
    
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    
    
    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];

    
	// Do any additional setup after loading the view.
    
    
    
    self.mapView.layer.shadowOpacity = 0.75f;
    self.mapView.layer.shadowRadius = 10.0f;
    self.mapView.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    self.mapView.layer.shadowOpacity = 0.75f;
    self.mapView.layer.shadowRadius = 10.0f;
    self.mapView.layer.shadowColor = [UIColor blackColor].CGColor;
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    
    
    
    
    
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
    }
    
    // locationManager = [[CLLocationManager alloc] init];
    //[locationManager setDelegate:self];
    
    //[locationManager setDistanceFilter:kCLDistanceFilterNone];
    // self->locationManager.distanceFilter = 10.0f;
    //self->locationManager.desiredAccuracy = 5000;
    
    //  [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //[locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
    //[self->locationManager startUpdatingLocation];
    //  ^Trying to get it to not return to center
    
    
    
    
    [self.mapView setShowsUserLocation:YES];
    
    GreenCitiesService *greenService = [[GreenCitiesService alloc] init];
    
    self.greenCities = [greenService getGreenCities];
    
    
    
    NSLog(@"viewdidload fired!");
    
    
    
    
}


- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    NSLog(@"error = %@", [error description]);
}

- (void)mapView:(MKMapView *)mv didUpdateUserLocation:(MKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation fired!");
    
    CLLocationCoordinate2D maxCoord = {-90.0f,-180.0f};
    CLLocationCoordinate2D minCoord = {90.0f, 180.0f};
    
    for(int i = 0; i<=[self.greenCities count] - 1;i++)
    {
        GreenCity *gCity = (GreenCity *) [self.greenCities objectAtIndex:i];
        CLLocationCoordinate2D newCoord = { gCity.latitude, gCity.longitude };
        
        if(gCity.longitude > maxCoord.longitude)
        {
            maxCoord.longitude = gCity.longitude;
        }
        
        if(gCity.latitude > maxCoord.latitude)
        {
            maxCoord.latitude = gCity.latitude;
        }
        
        if(gCity.longitude < minCoord.longitude)
        {
            minCoord.longitude = gCity.longitude;
        }
        
        if(gCity.latitude < minCoord.latitude)
        {
            minCoord.latitude = gCity.latitude;
        }
        if ([mapView.annotations count] > 600) return;
        
        GreenCityAnnotation *annotation = [[GreenCityAnnotation alloc] initWithCoordinate:newCoord title:gCity.name subTitle:gCity.rank];
        annotation.item = gCity;
        
        
        [mv addAnnotation:annotation];
        
        
    }
    
}


#pragma mark -
#pragma mark Annotations Functions
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
    if (annotation==self.mapView.userLocation)
        return nil;
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MyPin"];
    
    annView.animatesDrop= NO;
    annView.canShowCallout = YES;
    [annView setSelected:YES];
    
    annView.pinColor = MKPinAnnotationColorRed;
    annView.calloutOffset = CGPointMake(0, 0);
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    btn.tintColor = [UIColor darkGrayColor];
    annView.rightCalloutAccessoryView=btn;
    
    
    NSLog(@"Animated Map Pins Dropped");
    
    return annView;
    // counterPlus++;
    //counterPlus = 2;
    //NSLog(@"Incremented Counter++");
    //if (counterPlus == 2)
    //  break;
    
    
}




- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    //
    //    MKAnnotationView *aV;
    //
    //    for (aV in views) {
    //
    //        // Don't pin drop if annotation is user location
    //        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
    //            continue;
    //        }
    //
    //        // Check if current annotation is inside visible map rect, else go to next one
    //        MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
    //        if (!MKMapRectContainsPoint(self.mapView.visibleMapRect, point)) {
    //            continue;
    //        }
    //
    //        CGRect endFrame = aV.frame;
    //
    //        // Move annotation out of view
    //        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
    //
    //        // Animate drop
    //        [UIView animateWithDuration:0.0 delay:0.0*[views indexOfObject:aV] options: UIViewAnimationOptionCurveLinear animations:^{
    //            aV.frame = endFrame;
    //            // Animate squash
    //        }completion:^(BOOL finished){
    //            if (finished) {
    //                [UIView animateWithDuration:0.05 animations:^{
    //                    aV.transform = CGAffineTransformMakeScale(1.0, 0.8);
    //                }completion:^(BOOL finished){
    //                    if (finished) {
    //                        [UIView animateWithDuration:0.1 animations:^{
    //                            aV.transform = CGAffineTransformIdentity;
    //                        }];
    //                    }
    //
    //                    [self->locationManager stopUpdatingLocation];
    //                     [locationManager stopUpdatingLocation];
    //
    //
    //                }];
    //            }
    //        }];
    //    }
    //
    [self->locationManager stopUpdatingLocation];
}



- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
	GreenCity *item =  ((GreenCityAnnotation*)view.annotation).item;
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail View"];

    ((DetailViewController *) newTopViewController).listingTitle = item.name;
    ((DetailViewController *) newTopViewController).comingFromMapPin = YES;

    NSLog(@"IHA item.title = %@", item.name);
    
    

//    if (self.view.window)
//    {
//        EnterPasscodeViewController_iPad *epvc = [[EnterPasscodeViewController_iPad alloc] initWithNibName:@"EnterPasscodeViewController_iPad" bundle:nil];
//        
//        [epvc setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
//        [self.navigationController presentModalViewController:epvc animated:YES];
//    }
    
    
//    [self.slidingViewController.topViewController performSegueWithIdentifier:@"MapToDetailSegue" sender:nil];

    [self.navigationController pushViewController:newTopViewController animated:YES];
    
//    [self presentViewController:newTopViewController animated:YES completion:nil];
    
//    newTopViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
  //  [self.slidingViewController.topViewController presentViewController:newTopViewController animated:YES completion:nil];
    
    
//    CGRect frame = self.slidingViewController.topViewController.view.frame;
//    self.slidingViewController.topViewController = newTopViewController;
//    self.slidingViewController.topViewController.view.frame = frame;
//    [self.slidingViewController resetTopView];
//
//    // CLLocationCoordinate2D loc = [MKUserLocation coordinate];
//    // MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 500, 500);
//    //[self.mapView setRegion:region animated:YES];
//    NSLog(@"Annotation View Updated!");
}


/*
 *  locationManager:didFailWithError:
 *
 *  Discussion:
 *    Invoked when an error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"%s, error = %@", __PRETTY_FUNCTION__, [error description]);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"%s, status = %d", __PRETTY_FUNCTION__, status);
}






- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    //MKCoordinateRegion region;
    //CLLocationCoordinate2D centerCoordinate;
    
    //centerCoordinate=mapView.region.center;
    //region.center= centerCoordinate;
    //NSLog(@" %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
    //MKCoordinateRegionMakeWithDistance(centerCoordinate,centerCoordinate.latitude, centerCoordinate.longitude);
    
    
    
    
    
    //[locationManager stopUpdatingLocation];
    
    
    
    
    
    //[mapView setCenterCoordinate:centerCoordinate];
    //[self->locationManager stopUpdatingLocation];
    
    
    //  [locationManager stopUpdatingLocation];
    
    //  [self.mapView.userLocation removeObserver:self forKeyPath:@"location"];
    //make within 500, 500.
    //[locationManager stopUpdatingLocation];
    // locationManager.delegate = nil;
    
    //[mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:YES];
    //  mapView.showsUserLocation=NO;
    //[self->locationManager stopUpdatingLocation];
    NSLog(@"drag started");
    return YES;
}


- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        
  
        
        NSLog(@"drag ended");
    }
}


- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}




@end


