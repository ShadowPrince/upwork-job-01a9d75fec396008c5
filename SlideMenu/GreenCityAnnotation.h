//
//  GreenCityAnnotation.h
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"
#import "GreenCity.h"

@interface GreenCityAnnotation : NSObject<MKAnnotation> {
    
    NSString *title; 
    CLLocationCoordinate2D coodinate; 
    NSString *subtitle;
    
    
}

-(id) initWithCoordinate:(CLLocationCoordinate2D) c title:(NSString *) t subTitle:(NSString *) st; 

@property (nonatomic,readonly) CLLocationCoordinate2D coordinate; 
@property (nonatomic,copy) NSString *title; 
@property (nonatomic,retain) NSString *subtitle; 
@property (nonatomic,retain) GreenCity *item;
@end
