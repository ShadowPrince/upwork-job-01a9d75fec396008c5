//
//  Tweet.h
//  ParsingXMLTutorial
//
//  Created by kent franks on 3/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Tweet : NSObject

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *link;

@end
