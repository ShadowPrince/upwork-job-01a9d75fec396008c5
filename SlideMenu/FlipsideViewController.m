//
//  FlipsideViewController.m
//  Around Me
//
//  Created by Jean-Pierre Distler on 30.01.13.
//  Copyright (c) 2013 Jean-Pierre Distler. All rights reserved.
//

#import "FlipsideViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"

#import "Place.h"
#import "PlacesLoader.h"
#import "MarkerView.h"

NSString * const kPhoneKey = @"formatted_phone_number";
NSString * const kWebsiteKey = @"website";

const int kInfoViewTag = 1001;


//Midnite Pottery - 20% Off    34.288876  -88.712897
//Journal, Inc - 10% Off    34.239814  -88.709441
//Papa Johns
//Southern Cloth - 10% off   34.256897 -88.720394
//La Vino - 10% Off Wine     34.280568
//Rebelanes - 20% off 34.251687, -88.714203


//@protocol FlipsideViewControllerDelegate
//- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
//@end

//@interface FlipsideViewController : UIViewController <ARLocationDelegate, ARDelegate, ARMarkerDelegate>

//@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

//add above ^ to .h. Connect Button in interface builder to menubtn.
@interface FlipsideViewController () <MarkerViewDelegate>

@property (nonatomic, strong) AugmentedRealityController *arController;
@property (nonatomic, strong) NSMutableArray *geoLocations;


@property (nonatomic, strong) NSMutableArray *poiCoords;
@property (nonatomic, strong) NSMutableArray *StaticLat;
@property (nonatomic, strong) NSMutableArray *StaticLong;

@end


@implementation FlipsideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self setTransform:CGAffineTransformMakeRotation(angle * M_PI / -180.0)];
    // NOTE: we're using the negative of the angle here
    //[self.myCallOutView setTransform:CGAffineTransformMakeRotation(- angle * M_PI / -180.0)]];
 
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:)
     //                                            name:UIDeviceOrientationDidChangeNotification object:nil];
    
   // UIViewController *c = [[UIViewController alloc]init];
   // [self presentModalViewController:c animated:NO];
   // [self dismissModalViewControllerAnimated:NO];
   // [c release];
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
	if(!_arController) {
		_arController = [[AugmentedRealityController alloc] initWithView:[self view] parentViewController:self withDelgate:self];
	}
	
	[_arController setMinimumScaleFactor:0.5];
	[_arController setScaleViewsBasedOnDistance:YES];
	[_arController setRotateViewsBasedOnPerspective:YES];
	[_arController setDebugMode:NO];
    
    //20% Off - Midnite Pottery     34.288876  -88.712897
    //10% Off Advertising - Daily Journal    34.239814  -88.709441
    //Papa Johns
    //10% off - Southern Cloth   34.256897 -88.720394
    //10% Off Wine - La Vino -      34.280568 -88.714933
    //20% off - Rebelanes  34.251687, -88.714203
    
    CLLocationCoordinate2D poiCoords[] = {{34.239814, -88.709441},
       // {34.239814, -88.709441},
        {34.256897, -88.720394},
        {34.280568, -88.714933},
       {34.2442282, -88.8057198},
        
       {34.297783, -88.703935},
       // {34.257974, -88.705622},
       // {34.223621, -88.718953},
       // {34.283378, -88.715071},
       // {34.246734, -88.716601},
       // {34.223255, -88.714203},
        
        
        {34.251687, -88.714203}};
    
    const char *poiNames[] = {
       // "20% Off - Midnite Pottery",  34.288876, -88.712897
        "10% Off Advertising - Daily Journal",
        "10% Off - Southern Cloth",
        "10% Off Wine - La Vino",
        "10% Off Boarding - Animal Care Center of Tupelo",
        "15% Off - Thai Garden",
        //"10% Off Wine - La Vino",
       // "50% Off - Tupelo Automobile Museum",
        
       // "1 Hour Free - Clayton O'Donnell, PLLC",
       // "5% Off - Tupelo Diesel Service",
       // "$5 to $20 Off -  Xpress Lube",
       // "5% Off - Dossett Big 4",
        //"10% Off - Dwayne Blackmon Chevrolet",
        
        
        "20% Off - Rebelanes"};
    
    
    
    [self setGeoLocations:[NSMutableArray arrayWithCapacity:[_locations count]]];
    for (int i=0; i<5; i++) {
        ARGeoCoordinate *coordinate = [ARGeoCoordinate coordinateWithLocation:[[CLLocation alloc]initWithLatitude:poiCoords[i].latitude longitude:poiCoords[i].longitude] locationTitle:[NSString stringWithCString:poiNames[i]]];
        [coordinate calibrateUsingOrigin:[_userLocation location]];
        MarkerView *markerView = [[MarkerView alloc] initWithCoordinate:coordinate delegate:self];
        [coordinate setDisplayView:markerView];
        [_arController addCoordinate:coordinate];
        [_geoLocations addObject:coordinate];
    }
    
    //    CLLocationCoordinate2D poiCoords[] = {{40.7711329, -73.9741874},
    //        {37.7690400, -122.4835193},
    //        {32.7343822, -117.1441227},
    //        {51.5068670, -0.1708030},
    //        {45.5126399, -73.6802448},
    //        {40.4152519, -3.6887466}};
    //    const char *poiNames[] = {
    //        "Central Park NY",
    //        "Golden Gate Park SF",
    //        "Balboa Park SD",
    //        "Hyde Park UK",
    //        "Mont Royal QC",
    //        "Retiro Park ES"};
    //    [self setGeoLocations:[NSMutableArray arrayWithCapacity:[_locations count]]];
    //    for (int i=0; i<6; i++) {
    //        ARGeoCoordinate *coordinate = [ARGeoCoordinate coordinateWithLocation:[[CLLocation alloc]initWithLatitude:poiCoords[i].latitude longitude:poiCoords[i].longitude] locationTitle:[NSString stringWithCString:poiNames[i]]];
    //        [coordinate calibrateUsingOrigin:[_userLocation location]];
    //        MarkerView *markerView = [[MarkerView alloc] initWithCoordinate:coordinate delegate:self];
    //        [coordinate setDisplayView:markerView];
    //        [_arController addCoordinate:coordinate];
    //        [_geoLocations addObject:coordinate];
    //    }
    
}

- (void)viewWillAppear:(BOOL)animated {
	[self generateGeoLocations];
}





//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  //  return NO;
//}

//- (void) didRotate:(NSNotification *)notification {
  //  int ori=1;
    //UIDeviceOrientation currOri = [[UIDevice currentDevice] orientation];
    //if ((currOri == UIDeviceOrientationLandscapeLeft) || (currOri == UIDeviceOrientationLandscapeRight)) ori=0;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions
- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[self delegate] flipsideViewControllerDidFinish:self];
}

- (void)generateGeoLocations {
	[self setGeoLocations:[NSMutableArray arrayWithCapacity:[_locations count]]];
	
	for(Place *place in _locations) {
		ARGeoCoordinate *coordinate = [ARGeoCoordinate coordinateWithLocation:[place location] locationTitle:[place placeName]];
		[coordinate calibrateUsingOrigin:[_userLocation location]];
		MarkerView *markerView = [[MarkerView alloc] initWithCoordinate:coordinate delegate:self];
		NSLog(@"Marker view %@", markerView);
		
		[coordinate setDisplayView:markerView];
		[_arController addCoordinate:coordinate];
		[_geoLocations addObject:coordinate];
	}
}

#pragma mark - ARLocationDelegate

-(NSMutableArray *)geoLocations {
	if(!_geoLocations) {
		[self generateGeoLocations];
	}
	return _geoLocations;
}

- (void)locationClicked:(ARGeoCoordinate *)coordinate {
	NSLog(@"Tapped location %@", coordinate);
}

#pragma mark - ARDelegate

-(void)didUpdateHeading:(CLHeading *)newHeading {
	
}

-(void)didUpdateLocation:(CLLocation *)newLocation {
	
}

-(void)didUpdateOrientation:(UIDeviceOrientation)orientation {
	
}

#pragma mark - ARMarkerDelegate

-(void)didTapMarker:(ARGeoCoordinate *)coordinate {
}

- (void)didTouchMarkerView:(MarkerView *)markerView {
	ARGeoCoordinate *tappedCoordinate = [markerView coordinate];
	CLLocation *location = [tappedCoordinate geoLocation];
	
	//int index = [_locations indexOfObjectPassingTest:^(id obj, NSUInteger index, BOOL *stop) {
	//	return [[obj location] isEqual:location];
	//}];
    
    
    int index = [_locations indexOfObjectPassingTest:^(Place *obj, NSUInteger index, BOOL *stop) {
        return [[obj location] isEqual:location];
    }];
    
	
	if(index != NSNotFound) {
		Place *tappedPlace = [_locations objectAtIndex:index];
		[[PlacesLoader sharedInstance] loadDetailInformation:tappedPlace successHanlder:^(NSDictionary *response) {
			NSLog(@"Response: %@", response);
            NSDictionary *resultDict = [response objectForKey:@"result"];
			[tappedPlace setPhoneNumber:[resultDict objectForKey:kPhoneKey]];
			[tappedPlace setWebsite:[resultDict objectForKey:kWebsiteKey]];
			[self showInfoViewForPlace:tappedPlace];
		} errorHandler:^(NSError *error) {
			NSLog(@"Error: %@", error);
		}];
	}
}


- (void)showInfoViewForPlace:(Place *)place {
	CGRect frame = [[self view] frame];
	UITextView *infoView = [[UITextView alloc] initWithFrame:CGRectMake(50.0f, 50.0f, frame.size.width - 100.0f, frame.size.height - 100.0f)];
	[infoView setCenter:[[self view] center]];
	[infoView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
	[infoView setText:[place infoText]];
	[infoView setTag:kInfoViewTag];
	[infoView setEditable:NO];
	[[self view] addSubview:infoView];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UIView *infoView = [[self view] viewWithTag:kInfoViewTag];
	
	[infoView removeFromSuperview];
}

@end
