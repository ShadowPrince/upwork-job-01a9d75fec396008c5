//
//  GreenCityAnnotationView.m
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import "GreenCityAnnotationView.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"

@implementation GreenCityAnnotationView

@synthesize rank; 

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self != nil)
    {
        CGRect frame = self.frame;
        frame.size = CGSizeMake(30, 30.0);
        self.frame = frame;
        self.backgroundColor = [UIColor greenColor];
       // self.centerOffset = CGPointMake(-5, -5);
    }
    return self;
}

-(void) drawRect:(CGRect)rect
{
    
    UILabel *label = [[UILabel alloc] initWithFrame: rect];
    label.backgroundColor = [UIColor greenColor];
    label.text = self.rank;
    [self addSubview:label];
   // [label release];
    
   // [[UIImage imageNamed:@"house.png"] drawInRect:CGRectMake(30, 30.0, 30.0, 30.0)];
}

@end
