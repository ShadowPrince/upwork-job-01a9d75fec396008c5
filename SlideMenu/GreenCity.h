//
//  GreenCity.h
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ECSlidingViewController.h"
//#import "MenuViewController.h"
//#import "EleventhViewController.h"

@interface GreenCity : NSObject

- (id)initWithInfo:(NSString *)n latitude:(CGFloat)lat longitude:(CGFloat)lon;

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *name;
@property (assign, nonatomic) CGFloat latitude;
@property (assign, nonatomic) CGFloat longitude;
@property (copy, nonatomic) NSString *rank;
@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *website;
@property (copy, nonatomic) NSString *address_1;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *zip;
@property (copy, nonatomic) NSString *discount;
@property (copy, nonatomic) NSString *link;

@end
