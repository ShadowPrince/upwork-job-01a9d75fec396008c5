//
//  GreenCityAnnotation.m
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import "GreenCityAnnotation.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"


@implementation GreenCityAnnotation

@synthesize title,coordinate,subtitle, item;

-(id) initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t subTitle:(NSString *)st 
{
   self = [super init];
    coordinate = c; 
    [self setTitle:t];
    [self setSubtitle:st];

    
    return self; 
}

@end
