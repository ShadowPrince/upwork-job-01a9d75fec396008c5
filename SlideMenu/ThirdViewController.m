//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "ThirdViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "RSSParserFirstLevel.h"
#import "RSSSecondLevelViewController.h"


@interface ThirdViewController ()

@property (strong, nonatomic) NSArray *listingsFirstLevel;

@end


@implementation ThirdViewController

//XMLParser *xmlParser;
UIImage	*twitterLogo;
CGRect dateFrame;
UILabel *dateLabel;
CGRect contentFrame;
UILabel *contentLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
 
    
    //Try to dismiss keyboard on tableView
   // UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    //[self.tweetsTableView addGestureRecognizer:gestureRecognizer];
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
//    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    menuBtn.frame = CGRectMake(9, 23, 40, 30);
//    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:self.menuBtn];
    
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    //Top Main Menu Search Button
//    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    searchBtn.frame = CGRectMake(275, 25, 40, 30);
//    [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:self.searchBtn];
    
    //self.view.delegate = self;
    
    
//    self.xmlParser = [[XMLParser alloc] loadXMLByURL:@"http://www.cdfms.org/chamber/chamberadvantage/rss"];
    
    twitterLogo = [UIImage imageNamed:@"twitter-logo.png"];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.listingsFirstLevel = [defaults arrayForKey:@"ListingsFirstLevel"];
    
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    self.listingsFirstLevel = (NSArray *) [defaults arrayForKey:@"ListingsFirstLevel"];
//    
//    if (!self.listingsFirstLevel)
//    {
//        NSLog(@"self.listingsFirstLevel == nil");
//        
//        self.xmlParser = [[RSSParserFirstLevel alloc] loadXMLByURL:@"http://www.cdfms.org/chamber/chamberadvantage/rss"];
//        
//        self.listingsFirstLevel = [defaults arrayForKey:@"ListingsFirstLevel"];
//    }
//    else
//    {
//        NSLog(@"self.listingsFirstLevel != nil");
//    }
//    
//    NSLog(@"self.listingsFirstLevel == %@", self.listingsFirstLevel);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [self.xmlParser.tweets count];
    return [self.listingsFirstLevel count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
//	Tweet *currentTweet = [self.xmlParser.tweets objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        
//        UIImage	 *twitterLogo = [UIImage imageNamed:@"twitter-logo.png"];
//        
//        CGRect imageFrame = CGRectMake(2, 8, 40, 40);
//        self.customImage = [[UIImageView alloc] initWithFrame:imageFrame];
//        self.customImage.image = twitterLogo;
//        [cell.contentView addSubview:self.customImage];
        cell.imageView.image = twitterLogo;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
//        CGRect contentFrame = CGRectMake(45, 2, 265, 30);
//        UILabel *contentLabel = [[UILabel alloc] initWithFrame:contentFrame];
//        contentLabel.tag = 0011;
//        contentLabel.numberOfLines = 2;
//        contentLabel.font = [UIFont boldSystemFontOfSize:12];
//        [cell.contentView addSubview:contentLabel];
//        
//        CGRect dateFrame = CGRectMake(45, 40, 265, 10);
//        UILabel *dateLabel = [[UILabel alloc] initWithFrame:dateFrame];
//        dateLabel.tag = 0012;
//        dateLabel.font = [UIFont systemFontOfSize:10];
//        [cell.contentView addSubview:dateLabel];
    }
	
//	UILabel *contentLabel = (UILabel *)[cell.contentView viewWithTag:0011];
//    contentLabel.text = [currentTweet content];
//	
//	UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:0012];
//    dateLabel.text = [currentTweet dateCreated];
    
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:12];
//    cell.textLabel.text = currentTweet.title;
    cell.textLabel.text = [[self.listingsFirstLevel objectAtIndex:indexPath.row] objectForKey:@"TweetTitle"];
	
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 55;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    Tweet *currentTweet = [self.xmlParser.tweets objectAtIndex:indexPath.row];
//    NSLog(@"[currentTweet dateCreated] = %@", [currentTweet link]);
    
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    NSDictionary *dictionary = (NSDictionary *) [self.listingsFirstLevel objectAtIndex:indexPath.row];
    
    RSSSecondLevelViewController *secondLevelVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSS_SecondLevel"];
    
//    secondLevelVC.title = currentTweet.title;
//    secondLevelVC.RSS_Link = currentTweet.link;
    
    secondLevelVC.title = [dictionary objectForKey:@"TweetTitle"];;
    secondLevelVC.RSS_Link = [dictionary objectForKey:@"TweetLink"];
    
    NSLog(@"AAAAA [dictionary objectForKey:@TweetLink] = %@", [dictionary objectForKey:@"TweetLink"]);
    
    //self.slidingViewController.topViewController = newTopViewController;
    [self.navigationController pushViewController:secondLevelVC
                                         animated:YES];
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
