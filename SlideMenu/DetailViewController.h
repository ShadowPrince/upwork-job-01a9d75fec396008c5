//
//  DetailViewController.h
//  CDF Chamber Advantage
//
//  Created by Jack on 4/12/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "GreenCity.h"

typedef enum {
    DetailAlertViewTagCall,
    DetailAlertViewTagWebsite,
    DetailAlertViewTagAddress
}DetailAlertViewTag;


#define kAlertMessageMakeCall @"Are you sure you want to leave the application to call this number?"
#define kAlertMessageVisitWebsite @"Are you sure you want to leave the application to visit this website?"
#define kAlertMessageVisitAddress @"Are you sure you want to leave application to view this address?"

@interface DetailViewController : UIViewController<UIAlertViewDelegate>

@property (assign, nonatomic, getter = isComingFromMapPin) BOOL comingFromMapPin;

@property (strong, nonatomic) NSString *listingTitle;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property (strong, nonatomic) GreenCity *itemDetails;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblAddressTitle;
@property (strong, nonatomic) IBOutlet UIView *offerBase;


//offer segment
@property (weak, nonatomic) IBOutlet UIView *offerBaseView;
@property (strong, nonatomic) IBOutlet UILabel *lblOffer;


//discount base view
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UIView *discountBaseView;

//contact segment
@property (strong, nonatomic) IBOutlet UIView *contactBase;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *websiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end