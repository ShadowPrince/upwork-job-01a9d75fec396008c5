//  Created by Ti Simpson on 4/1/13
//  Copyright 2013 Infinite Views Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GreenCity.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import "EleventhViewController.h"


@interface RSSDetailXMLParser : NSObject<NSXMLParserDelegate> {
    GreenCity *gCity; 
    NSMutableArray *greenCities;
}

-(NSMutableArray *) getSubCategoryDetailsWithURL : (NSString *) urlString;

@property (nonatomic,retain) GreenCity *gCity; 
@property (nonatomic,retain) NSMutableArray *greenCities; 

@end
