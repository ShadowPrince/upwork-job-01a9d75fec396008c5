//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
 MenuAlertViewTagMobileWebSite
}MenuAlertViewTag;

#define kUrlMobileSite @"https://www.CDFMS.org"
#define kAlertMessageVisitMobileWebsite @"Are you sure you want to leave the application to visit the mobile website?"

@interface MenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIButton *SidewalkViewBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
