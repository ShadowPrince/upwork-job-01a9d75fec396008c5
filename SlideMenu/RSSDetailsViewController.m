//
//  RSSDetailsViewController.m
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/11/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import "RSSDetailsViewController.h"

#import "ECSlidingViewController.h"
#import "MenuViewController.h"


@interface RSSDetailsViewController ()

@end


@implementation RSSDetailsViewController

UIImage	 *twitterLogo;
CGRect dateFrame;
UILabel *dateLabel;
CGRect contentFrame;
UILabel *contentLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self. menuBtn.frame = CGRectMake(9, 23, 40, 30);
    [self.menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.menuBtn];
    
    
    
    //Top Main Menu Search Button
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.searchBtn.frame = CGRectMake(275, 25, 40, 30);
    [self.searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [self.searchBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.searchBtn];
    
    
//    self.xmlParser = [[XMLParser alloc] loadXMLByURL:self.RSS_Link];
    twitterLogo = [UIImage imageNamed:@"twitter-logo.png"];
    
    //self.title = @"Tweets";
    
    
//    NSLog(@"%@", [self.contents objectForKey:@"Title"]);
//    NSLog(@"%@", [self.contents objectForKey:@"Address1"]);
//    NSLog(@"%@", [self.contents objectForKey:@"Address2"]);
//    NSLog(@"%@", [self.contents objectForKey:@"City"]);
//    NSLog(@"%@", [self.contents objectForKey:@"State"]);
//    NSLog(@"%@", [self.contents objectForKey:@"Zip"]);
//    NSLog(@"%@", [self.contents objectForKey:@"Phone"]);
//    NSLog(@"%@", [self.contents objectForKey:@"Link"]);
    
//    NSLog(@"self.contents = %@", self.contents);
}

- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
