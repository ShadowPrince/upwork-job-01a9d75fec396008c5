//
//  GreenCity.m
//  GreenCities
//
//  Created by Mohammad Azam on 7/7/11.
//  Copyright 2011 HighOnCoding. All rights reserved.
//

#import "GreenCity.h"
//#import "ECSlidingViewController.h"
//#import "MenuViewController.h"
//#import "EleventhViewController.h"


@implementation GreenCity

//@synthesize name,latitude,longitude,rank;
//address2 deleted @synthesize phone,website,address_1,address_2,city,state,zip;
//@synthesize phone, website, address_1, city, state, zip, discount, title;

- (id)initWithInfo:(NSString *)n latitude:(CGFloat)lat longitude:(CGFloat)lon
{
    self = [super init];
    
    if (self)
    {
        self.name = n;
        self.latitude = lat;
        self.longitude = lon;
    }
    
    return self;
}
@end
