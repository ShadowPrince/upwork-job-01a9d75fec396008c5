//
//  MainViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "SecondViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"



@interface SecondViewController ()

@end


@implementation SecondViewController


@synthesize menuBtn;
@synthesize searchBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    AnitmationimageView.animationImages = [NSArray arrayWithObjects:
                                           [UIImage imageNamed:@"1.png"],
                                           [UIImage imageNamed:@"2.png"],
                                           [UIImage imageNamed:@"3.png"],
                                           [UIImage imageNamed:@"4.png"],
                                           [UIImage imageNamed:@"5.png"],
                                           [UIImage imageNamed:@"6.png"],
                                           [UIImage imageNamed:@"7.png"],
                                           [UIImage imageNamed:@"8.png"],
                                           [UIImage imageNamed:@"9.png"],
                                           [UIImage imageNamed:@"10.png"], nil];
    [AnitmationimageView setAnimationRepeatCount:1];
    AnitmationimageView.animationDuration = 3;
    [AnitmationimageView startAnimating];
    [self performSelector:@selector(delay1) withObject:nil afterDelay:2]; //was originally 3 before CDF meeting.
    
    
    // Dispose of any resources that can be recreated.
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(9, 23, 40, 30);
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.menuBtn];
    
    
    
    //Top Main Menu Search Button
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(275, 25, 40, 30);
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(revealSearch:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.searchBtn];
    
   // [self.view.delegate = self];
    

    
    [self.view addSubview:self.searchBtn];
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)delay1 {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [AnitmationimageView setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(delay2) withObject:nil afterDelay:1.0];
    
}

-(void)delay2 {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [Loadimageview setAlpha:1];
    [UIView commitAnimations];
    [self performSelector:@selector(delay3) withObject:nil afterDelay:1.5];
}

-(void)delay3 {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [Loadimageview setAlpha:0];
    [UIView commitAnimations];
}




-(IBAction)revealSearch:(id)sender {
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}


- (IBAction)revealMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}



@end
