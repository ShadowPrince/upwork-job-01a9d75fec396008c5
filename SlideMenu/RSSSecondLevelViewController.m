//
//  RSSSecondLevelViewController.m
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/11/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import "RSSSecondLevelViewController.h"

#import "ECSlidingViewController.h"
#import "MenuViewController.h"

#import "RSSParserSecondLevel.h"


@interface RSSSecondLevelViewController ()

@property (strong, nonatomic) NSXMLParser *parser;

//- (id)loadXMLByURL:(NSString *)urlString;

@property (strong, nonatomic) NSArray *listingsSecondLevel;

@property (strong, nonatomic) NSMutableString *currentNodeContent;
@property (strong, nonatomic) NSMutableDictionary *currentDictionary;
@property (assign, nonatomic, getter = isStatus) BOOL status;
@property (assign, nonatomic, getter = isNewRow) BOOL newRow;

@end


@implementation RSSSecondLevelViewController

UIImage	 *twitterLogo;
CGRect dateFrame;
UILabel *dateLabel;
CGRect contentFrame;
UILabel *contentLabel;

//- (NSMutableArray *)listingsSecondLevel {
//    if (!_listingsSecondLevel) {
//        _listingsSecondLevel = [[NSMutableArray alloc] init];
//    }
//    return _listingsSecondLevel;
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.labelTitle.text = self.title;
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
//    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    self. menuBtn.frame = CGRectMake(9, 23, 40, 30);
//    [self.menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self.view addSubview:self.menuBtn];
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    //Top Main Menu Search Button
//    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.searchBtn.frame = CGRectMake(275, 25, 40, 30);
//    [self.searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [self.searchBtn addTarget:self action:@selector(revealSearch:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:self.searchBtn];
    
//    [self loadXMLByURL:self.RSS_Link];
    
//    RSSDetailXMLParser *robject = [[RSSDetailXMLParser alloc] init];
//    self.listingsSecondLevel = [robject getSubCategoryDetailsWithURL:self.RSS_Link];
    
    twitterLogo = [UIImage imageNamed:@"twitter-logo.png"];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.listingsSecondLevel = [defaults arrayForKey:self.RSS_Link];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listingsSecondLevel count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    GreenCity *data = [self.listingsSecondLevel objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        cell.imageView.image = twitterLogo;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
	
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:12];
//	cell.textLabel.text = data.name;
    cell.textLabel.text = [[self.listingsSecondLevel objectAtIndex:indexPath.row] objectForKey:@"TweetTitle"];
    
    
//    NSLog(@"%@", [currentData objectForKey:@"Title"]);
//    NSLog(@"%@", [currentData objectForKey:@"Address1"]);
//    NSLog(@"%@", [currentData objectForKey:@"Address2"]);
//    NSLog(@"%@", [currentData objectForKey:@"City"]);
//    NSLog(@"%@", [currentData objectForKey:@"State"]);
//    NSLog(@"%@", [currentData objectForKey:@"Zip"]);
//    NSLog(@"%@", [currentData objectForKey:@"Phone"]);
//    NSLog(@"%@", [currentData objectForKey:@"Link"]);
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 55;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
//    Tweet *currentTweet = [[self.xmlParser tweets] objectAtIndex:indexPath.row];
//    NSLog(@"self.contents = %@", [self.contents objectAtIndex:indexPath.row]);

//    GreenCity *item = [self.listingsSecondLevel objectAtIndex:indexPath.row];
    
    UIViewController *newTopViewController;
    
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Detail View"];    
//    ((DetailViewController*)newTopViewController).itemDetails = item;
    
    ((DetailViewController *) newTopViewController).listingTitle = [[self.listingsSecondLevel objectAtIndex:indexPath.row] objectForKey:@"TweetTitle"];

    [self.navigationController pushViewController:newTopViewController
                                         animated:YES];
}

#pragma mark - View lifecycle
- (void)viewDidUnload
{
    [self setTweetsTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

//- (id)loadXMLByURL:(NSString *)urlString {
//    NSURL *url = [NSURL URLWithString:urlString];
//	NSData *data = [[NSData alloc] initWithContentsOfURL:url];
//    
//    self.parser = [[NSXMLParser alloc] initWithData:data];
//	self.parser.delegate = self;
//    [self.parser parse];
//	return self;
//}
//
//- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
//	self.currentNodeContent = (NSMutableString *) [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
////    NSLog(@"self.currentNodeContent = %@", self.currentNodeContent);
//}
//
//- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
//    if ([elementname isEqualToString:@"row"]) {
//        self.newRow = YES;
//        self.currentDictionary = [NSMutableDictionary dictionary];
//    }
//    
//    if ([elementname isEqualToString:@"item"]) {
//        self.status = YES;
//        self.newRow = NO;
//	}
//    
////	if ([elementname isEqualToString:@"description"])
//    if ([elementname isEqualToString:@"pubDate"]) {
//        self.status = NO;
//	}
//}
//
//- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementname namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
//{
//    if ([elementname isEqualToString:@"_rank"])
//    {
//        [self.currentDictionary setObject:self.currentNodeContent
//                                   forKey:@"Rank"];
//    }
//    
//    if (self.isStatus)
//    {
//        if ([elementname isEqualToString:@"title"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"Title"];
//        }
//        
//        if ([elementname isEqualToString:@"address_1"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"Address1"];
//        }
//        
//       // if ([elementname isEqualToString:@"address_2"])
//        //{
//          //  [self.currentDictionary setObject:self.currentNodeContent
//         //                              forKey:@"Address2"];
//        //}
//        
//        if ([elementname isEqualToString:@"city"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"City"];
//        }
//        
//        if ([elementname isEqualToString:@"state"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"State"];
//        }
//        
//        if ([elementname isEqualToString:@"zip"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"Zip"];
//        }
//        
//        if ([elementname isEqualToString:@"phone"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"Phone"];
//        }
//        
//        if ([elementname isEqualToString:@"link"])
//        {
//            [self.currentDictionary setObject:self.currentNodeContent
//                                       forKey:@"Link"];
//        }
//    }
//	
//    if ([elementname isEqualToString:@"item"])
//	{
//        [self.contents addObject:self.currentDictionary];
//        
//        self.currentDictionary = nil;
//		self.currentNodeContent = nil;
//	}
//    
//    
////    if (self.isStatus)
////    {
////        if ([elementname isEqualToString:@"title"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Title"];
////        }
////        
////        if ([elementname isEqualToString:@"address_1"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Address1"];
////        }
////        
////        if ([elementname isEqualToString:@"address_2"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Address2"];
////        }
////        
////        if ([elementname isEqualToString:@"city"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"City"];
////        }
////        
////        if ([elementname isEqualToString:@"state"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"State"];
////        }
////        
////        if ([elementname isEqualToString:@"zip"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Zip"];
////        }
////        
////        if ([elementname isEqualToString:@"phone"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Phone"];
////        }
////        
////        if ([elementname isEqualToString:@"link"])
////        {
////            [self.currentDictionary setObject:self.currentNodeContent
////                                       forKey:@"Link"];
////        }
////    }
////	
////    if ([elementname isEqualToString:@"item"])
////	{
////        [self.contents addObject:self.currentDictionary];
////        
////        self.currentDictionary = nil;
////		self.currentNodeContent = nil;
////	}
//}


- (IBAction)revealMenu:(id)sender
{
//    [self.slidingViewController anchorTopViewTo:ECRight];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];

    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

@end