//
//  RSSParserSecondLevel.h
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/26/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Tweet.h"


@interface RSSParserSecondLevel : NSObject <NSXMLParserDelegate>

@property (strong, readonly) NSMutableArray *tweets;

- (id)loadXMLByURL:(NSString *)urlString;

@end
