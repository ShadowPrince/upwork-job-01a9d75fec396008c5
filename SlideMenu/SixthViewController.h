//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SixthViewController : UIViewController

@property (strong, nonatomic) UIButton *menuBtn;
@property (strong, nonatomic) UIButton *searchBtn;

@end
