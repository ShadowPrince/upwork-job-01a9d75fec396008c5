//
//  AppDelegate.m
//
//
//  Created by Ti Simpson

#import "AppDelegate.h"
#import "Reachability.h"
#import <Parse/Parse.h>

#import "MBProgressHUD.h"

@interface AppDelegate ()

- (void)parseAndLoadRSSFeeds;

@end


@implementation AppDelegate

-(void)checkNetConnection {
    
    bool success = false;
    const char *host_name = [@"http://google.com"
                             cStringUsingEncoding:NSASCIIStringEncoding];
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL,                         host_name);
    SCNetworkReachabilityFlags flags;
    success = SCNetworkReachabilityGetFlags(reachability, &flags);
    bool isAvailable = success && (flags & kSCNetworkFlagsReachable) &&
    !(flags & kSCNetworkFlagsConnectionRequired);
    
    [[NSUserDefaults standardUserDefaults]setBool:isAvailable forKey:@"ISNETAVAILABLE"];
}


//- (void)parseAndLoadRSSFeeds
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//
//    self.listingsFirstLevel = (NSArray *) [defaults arrayForKey:@"ListingsFirstLevel"];
//
//    if (!self.listingsFirstLevel)
//    {
//        NSLog(@"self.listingsFirstLevel == nil");
//
//        self.firstLevelParser = [[RSSParserFirstLevel alloc] loadXMLByURL:@"http://www.cdfms.org/chamber/chamberadvantage/rss"];
//
//        self.listingsFirstLevel = [defaults arrayForKey:@"ListingsFirstLevel"];
//    }
//    else
//    {
//        NSLog(@"self.listingsFirstLevel != nil");
//    }
//
////    NSLog(@"self.listingsFirstLevel == %@", self.listingsFirstLevel);
//
//    for (NSDictionary *dictionary in self.listingsFirstLevel)
//    {
//        NSString *RSSLink = [dictionary objectForKey:@"TweetLink"];
//
//        self.secondLevelParser = [[RSSParserSecondLevel alloc] loadXMLByURL:RSSLink];
//
//        self.listingsSecondLevel = [defaults arrayForKey:@"ListingsSecondLevel"];
//
//        NSLog(@"self.listingsSecondLevel = %@", self.listingsSecondLevel);
//
//        [defaults setObject:self.listingsSecondLevel forKey:RSSLink];
//        [defaults synchronize];
//    }
//
//    [defaults setBool:YES
//               forKey:@"BasicListingsInfoDownloaded"];
//    [defaults setObject:[NSDate date]
//                 forKey:@"LastTimeBasicListingsInfoDownloaded"];
//    [defaults synchronize];
//
//    NSLog(@"DATE BasicListingsInfoDownloaded = %@", (NSDate *) [defaults objectForKey:@"LastTimeBasicListingsInfoDownloaded"]);
//
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self checkNetConnection];
    
    bool isAvailable = [[NSUserDefaults standardUserDefaults]boolForKey:@"ISNETAVAILABLE"];
    
    if (isAvailable)
    {
        // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Connection Detected"
        //                                               message:@"This application requires an active internet connection to correctly retrieve information. Please check your devices network settings."
        //                                            delegate:self
        //                                 cancelButtonTitle:@"Ok"
        //                               otherButtonTitles:nil];
        // [alert show];
    }
    else
    {
        // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TESTING INTERNET CONNECTION"
        //                                             message:@"This application requires an active internet connection. Please check your devices network settings."
        //                                        delegate:self
        //                           cancelButtonTitle:@"Ok"
        //                       otherButtonTitles:nil];
        //[alert show];
    }
    
    
    //    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
    //                         animated:YES];
    
    //
    //    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
    //                         animated:YES];
    //
    //    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    //        // Do something...
    //
    //        while (1) {
    //
    //        }
    //
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
    //                                 animated:YES];
    //        });
    //    });
    
    
    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    //        // Do something...
    //        while (1) {
    //
    //        }
    ////        //        }
    //
    //        [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
    //                             animated:YES];
    //    });
    
    
    
    //    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
    //                         animated:YES];
    //
    //    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    //        // Do something...
    //
    //        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //
    //        if ([defaults boolForKey:@"BasicListingsInfoDownloaded"] == NO)
    //        {
    //            [self parseAndLoadRSSFeeds];
    //        }
    //        else
    //        {
    //            double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
    //
    //            if (timeInterval > 60 * 60 * 24 * 3)
    //            {
    //                [self parseAndLoadRSSFeeds];
    //            }
    //        }
    //
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //
    //            [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
    //                                 animated:YES];
    //        });
    //    });
    
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //
    //    if ([defaults boolForKey:@"BasicListingsInfoDownloaded"] == NO)
    //    {
    //        [self parseAndLoadRSSFeeds];
    //    }
    //    else
    //    {
    //        double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
    //
    //        if (timeInterval > 60 * 60 * 24 * 3)
    //        {
    //            [self parseAndLoadRSSFeeds];
    //        }
    //    }
    
    
    
    
    
    // ****************************************************************************
    // Uncomment and fill in with your Parse credentials:
    [Parse setApplicationId:@"Fg9qoUueQsXkmnX13AqqpoN3TVaiQrnDAud55XaQ" clientKey:@"HbNq5Tylulv5SdcAqskdp0xkFu53cA0AVwXUbkes"];
    // ****************************************************************************
    
    // Override point for customization after application launch.
    //[application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    
    
    
    
    // Register for Parse push notifications
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    
    
    // Override point for customization after application launch.
    return YES;
}


- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:newDeviceToken];
    [currentInstallation saveInBackground];
    
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}


- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    //    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //
    //    [defaults removeObjectForKey:@"ListingsFirstLevel"];
    //    [defaults removeObjectForKey:@"ListingsSecondLevel"];
    //    [defaults synchronize];
}

@end
