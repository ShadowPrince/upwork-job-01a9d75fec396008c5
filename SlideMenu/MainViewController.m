//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//




// "Sidewalk View Main Implementation." POI's are displaed on FlipsideViewController View. Currently FlipSideViewController is initiated/launched by one of two buttons, one programmaically instated and one done via storyboard for testing/programming purposes. 


#import "MainViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import "Place.h"
#import "PlaceAnnotation.h"
#import "PlacesLoader.h"


#import "RSSParserFirstLevel.h"
#import "RSSParserSecondLevel.h"

#import "MBProgressHUD.h"




#define DOWNLOAD_LISTING_ALERT_TAG 10001



NSString * const kNameKey = @"name";
NSString * const kReferenceKey = @"reference";
NSString * const kAddressKey = @"vicinity";
NSString * const kLatiudeKeypath = @"geometry.location.lat";
NSString * const kLongitudeKeypath = @"geometry.location.lng";

@interface MainViewController () <CLLocationManagerDelegate, MKMapViewDelegate, UIAlertViewDelegate>



@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *locations;


@property (strong, nonatomic) RSSParserFirstLevel *firstLevelParser;
@property (strong, nonatomic) NSArray *listingsFirstLevel;

@property (strong, nonatomic) RSSParserSecondLevel *secondLevelParser;
@property (strong, nonatomic) NSArray *listingsSecondLevel;

- (void)parseAndLoadRSSFeeds;

@end

@implementation MainViewController
@synthesize SidewalkViewBtn;

- (void)parseAndLoadRSSFeeds
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.listingsFirstLevel = [defaults arrayForKey:@"ListingsFirstLevel"];
    
    if (!self.listingsFirstLevel)
    {
        NSLog(@"self.listingsFirstLevel == nil");
        
        self.firstLevelParser = [[RSSParserFirstLevel alloc] loadXMLByURL:@"http://www.cdfms.org/chamber/chamberadvantage/rss"];
        
        self.listingsFirstLevel = [defaults arrayForKey:@"ListingsFirstLevel"];
    }
    else
    {
        NSLog(@"self.listingsFirstLevel != nil");
    }
    
    //    NSLog(@"self.listingsFirstLevel == %@", self.listingsFirstLevel);
    
    for (NSDictionary *dictionary in self.listingsFirstLevel)
    {
        NSString *RSSLink = [dictionary objectForKey:@"TweetLink"];
        
        self.secondLevelParser = [[RSSParserSecondLevel alloc] loadXMLByURL:RSSLink];
        
        self.listingsSecondLevel = [defaults arrayForKey:@"ListingsSecondLevel"];
        
        NSLog(@"self.listingsSecondLevel = %@", self.listingsSecondLevel);
        
        [defaults setObject:self.listingsSecondLevel forKey:RSSLink];
        [defaults synchronize];
    }
    
    [defaults setBool:YES
               forKey:@"BasicListingsInfoDownloaded"];
    [defaults setObject:[NSDate date]
                 forKey:@"LastTimeBasicListingsInfoDownloaded"];
    [defaults synchronize];
    
    NSLog(@"DATE BasicListingsInfoDownloaded = %@", (NSDate *) [defaults objectForKey:@"LastTimeBasicListingsInfoDownloaded"]);
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
                         animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        
        [self parseAndLoadRSSFeeds];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
                                 animated:YES];
        });
    });

    
    
    
//    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
//                         animated:YES];
//    
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//        // Do something...
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        
//        if ([defaults boolForKey:@"BasicListingsInfoDownloaded"] == NO)
//        {
//            [self parseAndLoadRSSFeeds];
//        }
//        else
//        {
//            double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
//            
//            if (timeInterval > 60 * 60 * 24 * 3)
//            {
//                [self parseAndLoadRSSFeeds];
//            }
//        }
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
//                                 animated:YES];
//        });
//    });
}

- (IBAction)done:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"FIRED NEW NO CONNECTION DETECTED ON MAINVIEW");
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Alert" message: @"This application requires an active internet connection. Please check your devices network settings." delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show];
    
    
    } else {
        NSLog(@"There IS internet connection!!!!!!");
    }
    


    
//    bool isAvailable = [[NSUserDefaults standardUserDefaults]boolForKey:@"ISNETAVAILABLE"];
//    
//    if (isAvailable) {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Connection Detected"
//                                                        message:@"This application requires an active internet connection. Please check your devices network settings."
//                                                       delegate:self
//                                              cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//        
//        
//    }
//    
//    else
//    {
//        // internet is available.
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Money Money Money"
//                                                      message:@"This application requires an active internet connection to display proper information. Please check your devices network settings."
//                                                   delegate:self
//                                        cancelButtonTitle:@"Ok"
//                                      otherButtonTitles:nil];
//        
//        alert.tag = DOWNLOAD_LISTING_ALERT_TAG;
//        
//        [alert show];
//    }

    float width = self.view.bounds.size.width;
    float height = self.view.bounds.size.height;
    UIWebView *wv1 = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    wv1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    wv1.scalesPageToFit = NO;
    //wv1.delegate = self;
    [self.view insertSubview:wv1 aboveSubview:_mapView];
    
    
    wv1.backgroundColor = [UIColor whiteColor];
    wv1.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    [wv1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.infinite-views.com//CDF/iPhone/HomeScreen/Onload.html"]]];
    
    //http://www.infinite-views.com//CDF/iPhone/HomeScreen/CDFMS/CDFData/www.cdfms.org/index.html
    
    
    
    [self.menuBtn addTarget:self
                     action:@selector(revealMenu:)
           forControlEvents:UIControlEventTouchUpInside];
    
    //    [self.view addSubview:self.menuBtn];
    
    
    [self.mapBtn addTarget:self
                    action:@selector(revealMap:)
          forControlEvents:UIControlEventTouchUpInside];
    
    [self.tableBtn addTarget:self
                      action:@selector(revealTable:)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.searchBtn addTarget:self
                       action:@selector(revealSearch:)
             forControlEvents:UIControlEventTouchUpInside];
    
    
    //[wv1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.infinite-views.com//CDF/iPhone/HomeScreen/onload/"]]];
    //The Following Button Launches the FlipSideViewController Programatically

    
    
    //The Following Button Launches the FlipSideViewController Programatically
    
    [self setLocationManager:[[CLLocationManager alloc] init]];
    [_locationManager requestWhenInUseAuthorization];
	[_locationManager setDelegate:self];
	[_locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
	[_locationManager startUpdatingLocation];
    
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    
    
    //Top Main Menu Push Segue Button
  //  self.SidewalkViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
   // SidewalkViewBtn.frame = CGRectMake(200, 25, 40, 30);
    //[SidewalkViewBtn setBackgroundImage:[UIImage imageNamed:@"twitter-logo.png"] forState:UIControlStateNormal];
    //[SidewalkViewBtn addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //[self.view addSubview:self.SidewalkViewBtn];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
    
    if ([defaults boolForKey:@"BasicListingsInfoDownloaded"] == NO || timeInterval > 60 * 60 * 24 * 3)
    {
        bool isAvailable = [[NSUserDefaults standardUserDefaults] boolForKey:@"ISNETAVAILABLE"];
        
        if (isAvailable)
        {
           // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Connection Detected"
             //                                               message:@"This application requires an active internet connection. Please check your devices network settings."
               //                                            delegate:nil
                 //                                 cancelButtonTitle:@"Ok"
                   //                               otherButtonTitles:nil];
           // [alert show];
        }
        
        else
        {
            // internet is unavailable.
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification"
                                                            message:@"Chamber Business Data Will Now Be Updated"
                                                           delegate:self
                                                  cancelButtonTitle:@"Continue"
                                                  otherButtonTitles:nil];
            
            alert.tag = DOWNLOAD_LISTING_ALERT_TAG;
            
            [alert show];
        }
    }
    
    
    
    
    
//    [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication].windows lastObject]
//                         animated:YES];
//    
//    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//        // Do something...
//        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        
//        if ([defaults boolForKey:@"BasicListingsInfoDownloaded"] == NO)
//        {
//            [self parseAndLoadRSSFeeds];
//        }
//        else
//        {
//            double timeInterval = [[NSDate date] timeIntervalSinceDate:(NSDate *)[defaults objectForKey:@"LastTimeDownloaded"]];
//            
//            if (timeInterval > 60 * 60 * 24 * 3)
//            {
//                [self parseAndLoadRSSFeeds];
//            }
//        }
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            [MBProgressHUD hideHUDForView:[[UIApplication sharedApplication].windows lastObject]
//                                 animated:YES];
//        });
//    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)nextView{
    [self performSegueWithIdentifier:@"showAlternate" sender:self];
}
#pragma mark - CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	CLLocation *lastLocation = [locations lastObject];
	
	CLLocationAccuracy accuracy = [lastLocation horizontalAccuracy];
	
	NSLog(@"Received location %@ with accuracy %f", lastLocation, accuracy);
	//default 100
	if(accuracy < 1000.0) {
		MKCoordinateSpan span = MKCoordinateSpanMake(0.14, 0.14);
		MKCoordinateRegion region = MKCoordinateRegionMake([lastLocation coordinate], span);
		
		[_mapView setRegion:region animated:YES];
        //This starts loading a list of POIs that are within a radius of certain amount of meters of the user’s current position, and prints them to the console.
		[[PlacesLoader sharedInstance] loadPOIsForLocation:[locations lastObject] radius:200 successHanlder:^(NSDictionary *response) {
			NSLog(@"Response: %@", response);
			if([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
				id places = [response objectForKey:@"results"];
				NSMutableArray *temp = [NSMutableArray array];
				
				if([places isKindOfClass:[NSArray class]]) {
					for(NSDictionary *resultsDict in places) {
						CLLocation *location = [[CLLocation alloc] initWithLatitude:[[resultsDict valueForKeyPath:kLatiudeKeypath] floatValue] longitude:[[resultsDict valueForKeyPath:kLongitudeKeypath] floatValue]];
						Place *currentPlace = [[Place alloc] initWithLocation:location reference:[resultsDict objectForKey:kReferenceKey] name:[resultsDict objectForKey:kNameKey] address:[resultsDict objectForKey:kAddressKey]];
						[temp addObject:currentPlace];
						
						PlaceAnnotation *annotation = [[PlaceAnnotation alloc] initWithPlace:currentPlace];
						[_mapView addAnnotation:annotation];
					}
				}
                
				_locations = [temp copy];
				
				NSLog(@"Locations: %@", _locations);
			}
		} errorHandler:^(NSError *error) {
			NSLog(@"Error: %@", error);
		}];
		
		[manager stopUpdatingLocation];
	}
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
		[[segue destinationViewController] setLocations:_locations];
		[[segue destinationViewController] setUserLocation:[_mapView userLocation]];
    }
}



- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)revealMap:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Near Me"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealTable:(id)sender
{
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Listings"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
