//  Created by Ti Simpson on 4/1/13
//  Copyright 2013 Infinite Views Development. All rights reserved.

#import "EleventhViewController.h"
#import "RSSDetailXMLParser.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"

@implementation RSSDetailXMLParser


//CONSTANTS
/*
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
    <channel>
        <title>ChamberAdvantage</title>
        <link>http://www.cdfms.org/chamber/chamberadvantage</link>
        <description>RSS FEED</description>
        <row _id="3">
            <_rank>Tax Free Discount</_rank>
            <metro_area>The Cotton Bolt</metro_area>
            <item>
                <title>The Cotton Bolt</title>
                <address_1>1727 McCullough Blvd.</address_1>
                <address_2 />
                <city>Tupelo</city>
                <state>MS</state>
                <zip>38801</zip>
                <phone>662.841.2621</phone>
                <link>http://www.magnoliaco.com</link>
                <location latitude="34.285247" longitude="-88.739537">
                    <guid>http://www.magnoliaco.com</guid>
                    <pubDate>Tue, 23 Apr 2013 10:27:44 GMT</pubDate>
                </location>
            </item>
        </row>
    </channel>
</rss>
 */

/*
 CONSTANTS
 */

NSString * const ROW_ = @"row";
NSString * const TITLE_ = @"title";

NSString * const METRO_AREA_ = @"metro_area";
NSString * const RANK_ = @"_rank";
NSString * const LOCATION_ = @"location";
NSString * const PHONE_ = @"phone";
NSString * const LINK_ = @"link";
NSString * const ADDRESS_1_ = @"address_1";
//NSString * const ADDRESS_2 = @"address_2";
NSString * const CITY_ = @"city";
NSString * const STATE_ = @"state";
NSString * const ZIP_ = @"zip";
NSString * const DISCOUNT_ = @"discount";

@synthesize gCity,greenCities;

BOOL metroAreaFound;
BOOL titleFound;
BOOL rankFound;
BOOL phoneFound;
BOOL linkFound;
BOOL address_1Found;
//BOOL address_2Found;
BOOL cityFound;
BOOL stateFound;
BOOL zipFound;
BOOL discountFound;

-(NSMutableArray *) getSubCategoryDetailsWithURL : (NSString *) urlString;
{
    self.greenCities = [[NSMutableArray alloc] init];
    NSString *source = urlString;
    NSURL *url = [[NSURL alloc] initWithString:source];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [parser setDelegate:self];
    [parser parse];
    return self.greenCities;
}



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    
    if ([elementName isEqualToString:ROW_]) {
        self.gCity = [[GreenCity alloc] init];
    }
    else if([elementName isEqualToString:METRO_AREA_])
    {
        metroAreaFound = YES;
    }
    else if([elementName isEqualToString:TITLE_])
    {
        titleFound = YES;
    }
    else if([elementName isEqualToString:RANK_])
    {
        rankFound = YES;
    }
    else if([elementName isEqualToString:LOCATION_])
    {
        self.gCity.latitude =  [[attributeDict valueForKey:@"latitude"] floatValue];
        self.gCity.longitude = [[attributeDict valueForKey:@"longitude"] floatValue];
    }
    else if([elementName isEqualToString:PHONE_])
    {
        phoneFound = YES;
    }
    else if([elementName isEqualToString:LINK_])
    {
        linkFound = YES;
    }
    else if([elementName isEqualToString:ADDRESS_1_])
    {
        address_1Found = YES;
    }
    // else if([elementName isEqualToString:ADDRESS_2])
    //{
    //  address_2Found = YES;
    // }
    else if([elementName isEqualToString:CITY_])
    {
        cityFound = YES;
    }
    else if([elementName isEqualToString:STATE_])
    {
        stateFound = YES;
    }
    else if([elementName isEqualToString:ZIP_])
    {
        zipFound = YES;
    }
    else if([elementName isEqualToString:DISCOUNT_])
    {
        discountFound = YES;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(metroAreaFound)
    {
        self.gCity.name = string;
        metroAreaFound = NO;
    }
    else if (titleFound){
        self.gCity.title = string;
        titleFound = NO;
    }
    else if(rankFound)
    {
        self.gCity.rank = string;
        rankFound = NO;
    }
    else if (phoneFound) {
        self.gCity.phone = string;
        phoneFound = NO;
    }
    else if (linkFound)
    {
        self.gCity.website = string;
        linkFound = NO;
    }
    else if (address_1Found)
    {
        self.gCity.address_1 = string;
        address_1Found = NO;
    }
    //else if (address_2Found)
    //{
    //  self.gCity.address_2 = string;
    //address_2Found = NO;
    //}
    else if (cityFound)
    {
        self.gCity.city = string;
        cityFound = NO;
    }
    else if (stateFound)
    {
        self.gCity.state = string;
        stateFound = NO;
    }
    else if (zipFound)
    {
        self.gCity.zip = string;
        zipFound = NO;
    }else if (discountFound){
        self.gCity.discount = string;
        discountFound = NO;
    }
    //NSLog(@"%@",string);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:ROW_])
    {
        [self.greenCities addObject:self.gCity];
    }
    else if([elementName isEqualToString:TITLE_])
    {
        titleFound = YES;
    }
    else if ([elementName isEqualToString:PHONE_])
    {
        phoneFound = NO;
    }
    else if ([elementName isEqualToString:LINK_])
    {
        linkFound = NO;
    }
    else if ([elementName isEqualToString:ADDRESS_1_])
    {
        address_1Found = NO;
    }
    // else if ([elementName isEqualToString:ADDRESS_2])
    //{
    //  address_2Found = NO;
    //}
    else if ([elementName isEqualToString:CITY_])
    {
        cityFound = NO;
    }
    else if ([elementName isEqualToString:STATE_])
    {
        stateFound = NO;
    }
    else if ([elementName isEqualToString:ZIP_])
    {
        zipFound = NO;
    }
    else if ([elementName isEqualToString:RANK_])
    {
        rankFound = NO;
    }
    else if ([elementName isEqualToString:METRO_AREA_])
    {
        metroAreaFound = NO;
    }
    else if ([elementName isEqualToString:DISCOUNT_])
    {
        discountFound = NO;
    }
}


@end
