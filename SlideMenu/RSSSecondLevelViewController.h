//
//  RSSSecondLevelViewController.h
//  CDF Chamber Advantage
//
//  Created by App Dev Wizard on 4/11/14.
//  Copyright (c) 2014 Infinite Views Development LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tweet.h"
#import "DetailViewController.h"


@interface RSSSecondLevelViewController : UIViewController <NSXMLParserDelegate, UITableViewDataSource, UITableViewDelegate>

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *RSS_Link;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


//@property (strong, nonatomic) UIButton *menuBtn;
//@property (strong, nonatomic) UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property (strong, nonatomic) UIImageView *customImage;

@property (weak, nonatomic) IBOutlet UITableView *tweetsTableView;

@end
