//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FlipsideViewController.h"
#import "Reachability.h"
#define METERS_PER_MILE 1609.344

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate>


//@property (weak, nonatomic) IBOutlet MKMapView *mapView; //done in .m
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) UIButton *SidewalkViewBtn;
//FlipSide View Controller Done Button



@end
