//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ECSlidingViewController.h"
#import "MenuViewController.h"


@interface EleventhViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *greenCities;
    CLLocationManager *locationManager;
    
    // @private int counterPlus;
    
    
}
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic,retain) IBOutlet MKMapView *mapView;
@property (nonatomic,retain) NSMutableArray *greenCities;

//@property (strong, nonatomic) UIButton *stbackground;

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *tableBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@end


