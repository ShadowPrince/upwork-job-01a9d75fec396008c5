//
//  SecondViewController.h
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FlipsideViewController.h"

#define METERS_PER_MILE 1609.344

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate>


//@property (weak, nonatomic) IBOutlet MKMapView *mapView; //done in .m
@property (strong, nonatomic) UIButton *menuBtn;
@property (strong, nonatomic) UIButton *searchBtn;
//FlipSide View Controller Done Button



@end
