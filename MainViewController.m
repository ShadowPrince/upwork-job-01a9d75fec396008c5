//
//  SecondViewController.m
//  SlideMenu
//
//  Created by Kyle Begeman on 1/13/13.
//  Copyright (c) 2013 Indee Box LLC. All rights reserved.
//

#import "MainViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import "Place.h"
#import "PlaceAnnotation.h"
#import "PlacesLoader.h"

NSString * const kNameKey = @"name";
NSString * const kReferenceKey = @"reference";
NSString * const kAddressKey = @"vicinity";
NSString * const kLatiudeKeypath = @"geometry.location.lat";
NSString * const kLongitudeKeypath = @"geometry.location.lng";

@interface MainViewController () <CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *locations;


@end

@implementation MainViewController
@synthesize menuBtn;
@synthesize searchBtn;

- (IBAction)done:(id)sender {
   // [[self delegate] flipsideViewControllerDidFinish:self];
    [self dismissModalViewControllerAnimated:YES];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
   // _locationManager = [[CLLocationManager alloc] init];
//	[_locationManager setDelegate:self];
	//[_locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
//	[_locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
 //   [_locationManager startUpdatingLocation];
    
    [self setLocationManager:[[CLLocationManager alloc] init]];
	[_locationManager requestWhenInUseAuthorization];
	[_locationManager setDelegate:self];
	[_locationManager setDesiredAccuracy:kCLLocationAccuracyThreeKilometers];
	[_locationManager startUpdatingLocation];
    
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(9, 23, 40, 30);
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [menuBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.menuBtn];
    
    
    
    //Top Main Menu Search Button
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(275, 25, 40, 30);
    [searchBtn setBackgroundImage:[UIImage imageNamed:@"searchButton.png"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(revealMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.searchBtn];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	CLLocation *lastLocation = [locations lastObject];
	
	CLLocationAccuracy accuracy = [lastLocation horizontalAccuracy];
	
	NSLog(@"Received location %@ with accuracy %f", lastLocation, accuracy);
	//default 100
	if(accuracy < 1000.0) {
		MKCoordinateSpan span = MKCoordinateSpanMake(0.14, 0.14);
		MKCoordinateRegion region = MKCoordinateRegionMake([lastLocation coordinate], span);
		
		[_mapView setRegion:region animated:YES];
        //This starts loading a list of POIs that are within a radius of certain amount of meters of the user’s current position, and prints them to the console.
		[[PlacesLoader sharedInstance] loadPOIsForLocation:[locations lastObject] radius:150 successHanlder:^(NSDictionary *response) {
			NSLog(@"Response: %@", response);
			if([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
				id places = [response objectForKey:@"results"];
				NSMutableArray *temp = [NSMutableArray array];
				
				if([places isKindOfClass:[NSArray class]]) {
					for(NSDictionary *resultsDict in places) {
						CLLocation *location = [[CLLocation alloc] initWithLatitude:[[resultsDict valueForKeyPath:kLatiudeKeypath] floatValue] longitude:[[resultsDict valueForKeyPath:kLongitudeKeypath] floatValue]];
						Place *currentPlace = [[Place alloc] initWithLocation:location reference:[resultsDict objectForKey:kReferenceKey] name:[resultsDict objectForKey:kNameKey] address:[resultsDict objectForKey:kAddressKey]];
						[temp addObject:currentPlace];
						
						PlaceAnnotation *annotation = [[PlaceAnnotation alloc] initWithPlace:currentPlace];
						[_mapView addAnnotation:annotation];
					}
				}
                
				_locations = [temp copy];
				
				NSLog(@"Locations: %@", _locations);
			}
		} errorHandler:^(NSError *error) {
			NSLog(@"Error: %@", error);
		}];
		
		[manager stopUpdatingLocation];
	}
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
		[[segue destinationViewController] setLocations:_locations];
		[[segue destinationViewController] setUserLocation:[_mapView userLocation]];
    }
}

-(IBAction)revealSearch:(id)sender
{
    UIViewController *newTopViewController;
    newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Search"];
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}


- (IBAction)revealMenu:(id)sender
{
    NSLog(@"here coming ...");
    [self.slidingViewController anchorTopViewTo:ECRight];
}

@end
